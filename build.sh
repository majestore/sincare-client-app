#!/bin/bash

if [[ -z "$1" ]]; then
    echo "no params"
    exit
elif [[ $1 = "android" ]]; then
    echo "build platform: [ $1 ]"
    if [[ -z "$2" ]]; then
        ionic cordova build android
#        adb install -r ./platforms/android/app/build/outputs/apk/debug/app-debug.apk
    elif [[ $2 = "release" ]]; then
        ionic cordova build android --prod --release --buildConfig=build.json
#        adb install -r ./platforms/android/app/build/outputs/apk/release/app-release.apk
    else
        echo "second params need [ release ]"
    fi
elif [[ $1 = "ios" ]]; then
    echo "build platform: [ $1 ]"
    ionic cordova build ios
elif [[ $1 = "sign" ]]; then
    if [[ -z "$2" ]]; then
        echo "second params need [ sign jks file ]"
    else
        keytool -list -v -keystore $2
    fi
else
    echo "first params need [ android/ios ]"
fi
