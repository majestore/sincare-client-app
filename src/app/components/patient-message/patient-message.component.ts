import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-patient-message',
  templateUrl: './patient-message.component.html',
  styleUrls: ['./patient-message.component.scss'],
})
export class PatientMessageComponent implements OnInit {
  @Input() nameLabel: string;
  @Input() messageLabel: string;
  @Input() dateLabel: string;
  @Input() readLabel: string;
  @Input() orderAccept: number;
  @Input() pttEmail: string;
  @Input() doctorPhone: string;
  @Input() doctorAddress: string;
  @Input() sysLang: string;
  @Input() doctorType: number;

  constructor() { }

  ngOnInit() {}

}
