import {Component, OnInit} from '@angular/core';
import {ModalController, NavParams, PopoverController} from '@ionic/angular';
import {RegionPickModelComponent} from '../region-pick-model/region-pick-model.component';
import {HttpService} from '../../service/http.service';
import {RadioPickModelComponent} from '../radio-pick-model/radio-pick-model.component';

@Component({
    selector: 'app-filter-list',
    templateUrl: './filter-list.component.html',
    styleUrls: ['./filter-list.component.scss'],
})
export class FilterListComponent implements OnInit {

    filterList: Array<any> = [
        { label: '区域', value: 'regionId' },
        { label: '医院', value: 'hospitalId' },
        { label: '科室', value: 'departmentId' },
        { label: '查询', value: 'search' }
    ];
    checkedList: Array<any> = [];
    hospitalValue: null;
    departmentValue: null;

    constructor(
        private popoverController: PopoverController,
        private navParams: NavParams,
        private modalController: ModalController,
        private httpService: HttpService) {
        this.checkedList = navParams.get('checkedList') || [];
    }

    ngOnInit() {
        this.filterList.map(item => {
            const checkedItem: any = this.checkedList.find((subItem: any) => subItem.value === item.value);
            if (checkedItem) {
                item.checked = true;
                item.text = checkedItem.text;
                item.data = checkedItem.data;
            }
            return item;
        });
    }

    public async onItemSelect(item) {
        switch (item.value) {
            case 'regionId':
                if (item.checked) {
                    const regionResult = await this.showRegionModal();
                    if (regionResult) {
                        item.text = regionResult.label;
                        item.data = regionResult.value;

                        const hospitalItem = this.filterList.find(listItem => (listItem.value === 'hospitalId'));
                        if (hospitalItem) {
                            hospitalItem.checked = false;
                            hospitalItem.text = null;
                            hospitalItem.data = null;
                            this.hospitalValue = null;
                        }
                    } else {
                        item.checked = false;
                    }
                } else {
                    item.text = null;
                    item.data = null;

                    const hospitalItem = this.filterList.find(listItem => (listItem.value === 'hospitalId'));
                    if (hospitalItem) {
                        hospitalItem.checked = false;
                        hospitalItem.text = null;
                        hospitalItem.data = null;
                        this.hospitalValue = null;
                    }
                }
                break;

            case 'hospitalId':
                if (item.checked) {
                    const hospitalResult = await this.showHospitalModel();
                    if (hospitalResult) {
                        item.text = hospitalResult.label;
                        item.data = hospitalResult.value;
                    } else {
                        item.checked = false;
                    }
                } else {
                    item.text = null;
                    item.data = null;
                }
                break;

            case 'departmentId':
                if (item.checked) {
                    const departmentResult = await this.showDepartmentModel();
                    if (departmentResult) {
                        item.text = departmentResult.name;
                        item.data = departmentResult.value;
                    } else {
                        item.checked = false;
                    }
                } else {
                    item.text = null;
                    item.data = null;
                }
                break;

            case 'search':
                const checkedList = this.filterList.filter(filterItem => (filterItem.value !== 'search' && filterItem.checked))
                    .map(({ value, text, data }) => ({ value, text, data }));
                const checkedResult: any = {};
                checkedList.forEach(resultItem => {
                    const { value, data } = resultItem;
                    checkedResult[value] = data;
                });
                await this.popoverController.dismiss({ checkedList, checkedResult });
                break;
        }
    }

    private async showRegionModal() {
        const result: any = await this.httpService.get()
            .setPath('/pub/source/threeRegionData')
            .setQuery({
                style: 'tree'
            })
            .request();

        if (result.status === 0) {
            const modal = await this.modalController.create({
                component: RegionPickModelComponent,
                componentProps: {
                    title: '区域选择',
                    itemArray: [
                        {
                            name: '选择国家',
                            type: 1,
                        },
                        {
                            name: '选择州',
                            type: 2,
                        },
                        {
                            name: '选择城市',
                            type: 3
                        },
                    ],
                    dataArray: [ ...result.data ]
                }
            });
            await modal.present();
            const { data } = await modal.onDidDismiss();
            if (data) {
                const hospitalItem = this.filterList.find(listItem => (listItem.value === 'hospitalId' && listItem.checked));
                if (hospitalItem) {
                    hospitalItem.checked = false;
                    hospitalItem.text = null;
                    hospitalItem.data = null;
                }

                const [ , , region ] = data;
                return region;
            }
        }
    }

    private async showHospitalModel() {
        const queryParam: any = {
            listType: 'simple'
        };
        const regionItem = this.filterList.find(listItem => (listItem.value === 'regionId' && listItem.checked));
        if (regionItem) {
            queryParam.regionId = regionItem.data;
        }

        const result: any = await this.httpService.get()
            .setPath('/pub/source/hospitalList')
            .setQuery({
                ...queryParam
            })
            .request();

        if (result.status === 0) {
            const modal = await this.modalController.create({
                component: RadioPickModelComponent,
                componentProps: {
                    title: '选择医院',
                    dataList: [ ...result.data ],
                    selectValue: this.hospitalValue
                }
            });
            await modal.present();
            const { data } = await modal.onDidDismiss();
            if (data) {
                this.hospitalValue = data.value;
                return data;
            }
        }
    }

    private async showDepartmentModel() {
        const result: any = await this.httpService.get()
            .setPath('/pub/source/departmentList')
            .request();

        if (result.status === 0) {
            const modal = await this.modalController.create({
                component: RadioPickModelComponent,
                componentProps: {
                    title: '选择部门',
                    labelProp: 'name',
                    dataList: [ ...result.data ],
                    selectValue: this.departmentValue
                }
            });
            await modal.present();
            const { data } = await modal.onDidDismiss();
            if (data) {
                this.departmentValue = data.value;
                return data;
            }
        }
    }
}
