import {Component, OnInit} from '@angular/core';
import {ModalController, NavParams} from '@ionic/angular';

@Component({
    selector: 'app-service-model',
    templateUrl: './service-model.component.html',
    styleUrls: ['./service-model.component.scss'],
})
export class ServiceModelComponent implements OnInit {
    websiteLink = 'http://sincaremedicaltour.com/contact-us.php';

    constructor(private modalController: ModalController) {

    }

    ngOnInit() {
    }

    public async onModelClose() {
        await this.modalController.dismiss();
    }
}
