import {Component, OnInit} from '@angular/core';
import {ModalController, NavParams} from '@ionic/angular';
import {RegionListModelComponent} from './region-list-model/region-list-model.component';

@Component({
    selector: 'app-pick-model',
    templateUrl: './region-pick-model.component.html',
    styleUrls: ['./region-pick-model.component.scss'],
})
export class RegionPickModelComponent implements OnInit {

    title = null;
    fixedNation = null;
    itemArray: Array<{ name, type, label, value, disabled }> = [];
    nationArray: Array<{ value, label, children }> = [];
    provinceArray: Array<{ value, label, children }> = [];
    cityArray: Array<{ value, label }> = [];

    constructor(
        private modalController: ModalController,
        private navParams: NavParams
    ) {
        this.title = navParams.get('title');
        this.fixedNation = navParams.get('fixedNation');
        this.itemArray = navParams.get('itemArray');
        this.nationArray = this.navParams.get('dataArray');

        if (this.nationArray.length === 1) {
            const [ nationItem ] = this.nationArray;
            this.itemArray[0].value = nationItem.value;
            this.itemArray[0].label = nationItem.label;
            this.itemArray[0].disabled = true;
            this.provinceArray = nationItem.children;
            this.itemArray = this.itemArray.map(itemData => {
                const resultData = { ...itemData };
                if (itemData.type > 1) {
                    resultData.label = null;
                    resultData.value = null;
                    return resultData;
                } else {
                    return resultData;
                }
            });
        }
    }

    ngOnInit() {
    }

    public async onModelClose() {
        await this.modalController.dismiss();
    }

    public async onItemSelect(item) {
        console.log('onItemSelect:', item);
        if (item.type === 1) {
            const modal = await this.modalController.create({
                component: RegionListModelComponent,
                componentProps: {
                    title: item.name,
                    dataList: this.nationArray
                }
            });
            await modal.present();
            const { data } = await modal.onDidDismiss();
            if (data) {
                item.label = data.label;
                item.value = data.value;
                this.provinceArray = data.children;
                this.itemArray = this.itemArray.map(itemData => {
                    const resultData = { ...itemData };
                    if (itemData.type > 1) {
                        resultData.label = null;
                        resultData.value = null;
                        return resultData;
                    } else {
                        return resultData;
                    }
                });
            }
        } else if (item.type === 2 && this.itemArray[0].value) {
            const modal = await this.modalController.create({
                component: RegionListModelComponent,
                componentProps: {
                    title: item.name,
                    dataList: this.provinceArray
                }
            });
            await modal.present();
            const { data } = await modal.onDidDismiss();
            if (data) {
                item.label = data.label;
                item.value = data.value;
                this.cityArray = data.children;
                this.itemArray = this.itemArray.map(itemData => {
                    const resultData = { ...itemData };
                    if (itemData.type < 3) {
                        return resultData;
                    } else {
                        resultData.label = null;
                        resultData.value = null;
                        return resultData;
                    }
                });
            }
        } else if (item.type === 3 && this.itemArray[1].value) {
            const modal = await this.modalController.create({
                component: RegionListModelComponent,
                componentProps: {
                    title: item.name,
                    dataList: this.cityArray
                }
            });
            await modal.present();
            const { data } = await modal.onDidDismiss();
            if (data) {
                item.label = data.label;
                item.value = data.value;
            }
        }
    }

    public async onRegionConfirm() {
        const regionResult = this.itemArray.map(({ label, value }) => ({ label, value }));
        if (regionResult.every(item => !!item.label || !!item.value)) {
            await this.navParams.data.modal.dismiss(regionResult);
        }
    }
}
