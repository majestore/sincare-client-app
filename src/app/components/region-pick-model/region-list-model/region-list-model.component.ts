import {Component, OnInit} from '@angular/core';
import {ModalController, NavParams} from '@ionic/angular';

@Component({
    selector: 'app-list-model',
    templateUrl: './region-list-model.component.html',
    styleUrls: ['./region-list-model.component.scss'],
})
export class RegionListModelComponent implements OnInit {

    title: null;
    dataList: Array<{ value, label }> = [null];

    constructor(private modalController: ModalController, private navParams: NavParams) {
        this.title = navParams.get('title');
        this.dataList = navParams.get('dataList');
    }

    ngOnInit() {
    }

    public async onModelClose() {
        await this.modalController.dismiss();
    }

    public async onItemSelect(item) {
        await this.navParams.data.modal.dismiss({ ...item });
    }
}
