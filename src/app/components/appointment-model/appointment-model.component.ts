import { Component, OnInit } from '@angular/core';
import {ModalController, NavParams} from '@ionic/angular';

@Component({
    selector: 'app-appointment-model',
    templateUrl: './appointment-model.component.html',
    styleUrls: ['./appointment-model.component.scss'],
})
export class AppointmentModelComponent implements OnInit {

    title = null;
    cityAreaList = [];
    passportNoticeUrl = 'http://www.china-embassy.org/chn/lszj/zytz/t1841415.htm';
    noticeTitle = '关于进一步重申“非必要，非紧急，不旅行”以及调整核酸、血清抗体检测有关要求的重要通知';
    noticeContent = '注：微信支付，价格透明，我们代理您预约医院的双阴检测，额外收取35美金预约服务费，关于双阴检测费用由各个医院自行收取。';
    cityAreaContent = '六大直飞城市：';

    constructor(
        private modalController: ModalController,
        private navParams: NavParams
    ) {
        this.title = navParams.get('title');
        this.cityAreaList = navParams.get('cityAreaList');
    }

    ngOnInit() {
    }

    public async onCityAreaClick(row) {
        await this.modalController.dismiss({
            confirm: true,
            nationId: row.parent_id,
            cityAreaId: row.id
        });
    }

    public async onModelClose() {
        await this.modalController.dismiss();
    }

    public async onAppointmentHospital() {
        await this.modalController.dismiss({
            confirm: true
        });
    }
}
