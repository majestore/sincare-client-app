import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-img-modal',
  templateUrl: './img-modal.component.html',
  styleUrls: ['./img-modal.component.scss'],
})
export class ImgModalComponent implements OnInit {
  @Input() imgId: string;
  @Input() title: string;
  @Input() hospitalArr: any = [];
  @Input() personalRewards: string;
  @Input() personalInfo: string;
  @Input() medicalQualification: string;

  constructor(
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {}

  public async onModelConfirm() {
    await this.modalCtrl.dismiss();
  }

}
