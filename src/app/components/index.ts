import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import {TranslateModule} from '@ngx-translate/core';

import { RegionPickModelComponent } from './region-pick-model/region-pick-model.component';
import { RegionListModelComponent } from './region-pick-model/region-list-model/region-list-model.component';
import { RadioPickModelComponent } from './radio-pick-model/radio-pick-model.component';
import { CheckPickModelComponent } from './check-pick-model/check-pick-model.component';
import { CalendarModelComponent } from './calendar-model/calendar-model.component';
import { DoctorItemComponent } from './doctor-item/doctor-item.component';
import { OrderItemComponent } from './order-item/order-item.component';
import { FilterListComponent } from './filter-list/filter-list.component';
import { AgentModelComponent } from './agent-model/agent-model.component';
import { AppointmentModelComponent } from './appointment-model/appointment-model.component';
import { AppointHomeModelComponent } from './appoint-home-model/appoint-home-model.component';
import { FormsModule } from '@angular/forms';
import { CalendarModule } from 'ion2-calendar';
import { ServiceModelComponent } from './service-model/service-model.component';
import { PatientMessageComponent } from './patient-message/patient-message.component';
import { ImgModalComponent } from './img-modal/img-modal.component';

const CustomComponents = [
    RegionPickModelComponent,
    RegionListModelComponent,
    RadioPickModelComponent,
    CheckPickModelComponent,
    CalendarModelComponent,
    DoctorItemComponent,
    OrderItemComponent,
    FilterListComponent,
    AgentModelComponent,
    AppointmentModelComponent,
    AppointHomeModelComponent,
    ServiceModelComponent,
    PatientMessageComponent,
    ImgModalComponent
];

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        IonicModule.forRoot(),
        FormsModule,
        CalendarModule,
    ],
    declarations: [
        CustomComponents
    ],
    exports: [
        CustomComponents
    ],
    entryComponents: [
        CustomComponents
    ],
})
export class ComponentsModule {}
