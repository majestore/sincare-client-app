import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-doctor-item',
    templateUrl: './doctor-item.component.html',
    styleUrls: ['./doctor-item.component.scss'],
})
export class DoctorItemComponent implements OnInit {

    @Input() imageUrl: string;
    @Input() nameLabel: string;
    @Input() addressLabel: string;
    @Input() iconLabel: string;
    @Input() orderType: number;
    @Input() department: string;
    @Input() hasOrder: number;
    @Input() startDate: string;
    @Input() endDate: string;

    timeList = ['Contact', 'HasTime'];
    weekArray = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    showWeek = null;

    constructor() {
    }

    ngOnInit() {
        if (this.hasOrder > 0) {
            const startDate = new Date(this.startDate.replace(/-/g, '/'));
            this.showWeek = startDate.getDay();
        }
    }
}
