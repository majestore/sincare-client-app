import {Component, OnInit} from '@angular/core';
import {ModalController, NavParams} from '@ionic/angular';

@Component({
    selector: 'app-radio-pick-model',
    templateUrl: './radio-pick-model.component.html',
    styleUrls: ['./radio-pick-model.component.scss'],
})
export class RadioPickModelComponent implements OnInit {

    title: null;
    valueProp = null;
    labelProp = null;
    dataList: Array<any> = [];
    selectValue: number = null;

    constructor(private modalController: ModalController, private navParams: NavParams) {
        this.title = navParams.get('title');
        this.valueProp = navParams.get('valueProp') || 'value';
        this.labelProp = navParams.get('labelProp') || 'label';
        this.dataList = navParams.get('dataList');
        this.selectValue = navParams.get('selectValue');
    }

    ngOnInit() {
    }

    public async onModelConfirm() {
        await this.navParams.data.modal.dismiss(this.dataList.find(item => item[this.valueProp] === this.selectValue));
    }

    public async onModelDismiss() {
        await this.navParams.data.modal.dismiss();
    }
}
