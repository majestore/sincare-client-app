import {Component, OnInit} from '@angular/core';
import {ModalController, NavParams} from '@ionic/angular';

@Component({
    selector: 'app-check-pick-model',
    templateUrl: './check-pick-model.component.html',
    styleUrls: ['./check-pick-model.component.scss'],
})
export class CheckPickModelComponent implements OnInit {

    title: null;
    valueProp = null;
    labelProp = null;
    dataList: Array<any> = [];
    checkedList: Array<number> = [];

    constructor(private modalController: ModalController, private navParams: NavParams) {
        this.title = navParams.get('title');
        this.valueProp = navParams.get('valueProp') || 'value';
        this.labelProp = navParams.get('labelProp') || 'label';
        this.dataList = navParams.get('dataList');
        this.checkedList = navParams.get('checkedList') || [];
    }

    ngOnInit() {
        this.dataList = this.dataList.map(item => {
            const itemChecked = this.checkedList.includes(item[this.valueProp]);
            return {
                ...item,
                checked: itemChecked
            };
        });
    }

    public async onModelConfirm() {
        await this.navParams.data.modal.dismiss(this.dataList.filter(item => item.checked).map(item => item[this.valueProp]));
    }

    public async onModelDismiss() {
        await this.navParams.data.modal.dismiss();
    }
}
