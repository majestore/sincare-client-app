import {Component, OnInit} from '@angular/core';
import {ModalController, NavParams} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'app-calendar-model',
    templateUrl: './calendar-model.component.html',
    styleUrls: ['./calendar-model.component.scss'],
})
export class CalendarModelComponent implements OnInit {

    title = '';
    timeRange = false;
    patientQuery = false;
    dateValue = null;
    calendar = {
        format: 'YYYY-MM-DD',
        type: 'string',
        options: {
            color: 'primary',
            monthPickerFormat: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
            weekdays: ['日', 'M', 'T', 'W', 'T', 'F', 'S']
        }
    };
    sysLang = '';
    datetimeData: any = {
        'en-us': {
            cancelText: 'Cancel',
            doneText: 'Done',
        },
        'zh-cn': {
            cancelText: '取消',
            doneText: '确定',
        },
        'zh-tw': {
            cancelText: '取消',
            doneText: '確定',
        }
    };
    hoursRange = [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18];
    minuteRange = [0, 30];
    startTime: null;
    endTime: null;
    orderTimeRange: Array<any> = [];

    constructor(private modalController: ModalController, private navParams: NavParams, private translate: TranslateService) {
        this.title = navParams.get('title');
        this.timeRange = navParams.get('timeRange');
        this.patientQuery = navParams.get('patientQuery');
        this.orderTimeRange = navParams.get('orderTimeRange');
    }

    ngOnInit() {
        this.sysLang = this.translate.getBrowserCultureLang().toLowerCase();
    }

    public onChange($event) {
        console.log('onChange:', this.dateValue);
    }

    public onDatetimeChanged() {
        const startDate = new Date(this.startTime);
        const hours = startDate.getHours();
        const minute = startDate.getMinutes();

        if (hours === 18 && minute === 30) {
            this.hoursRange.push(19);
        } else {
            this.hoursRange.push();
        }
    }

    public onRangeChanged() {

    }

    public async onModelClose() {
        await this.modalController.dismiss();
    }

    private prefixZero(num, n = 2) {
        return (Array(n).join('0') + num).slice(-n);
    }

    public async onModelQuery() {
        if (this.dateValue) {
            const orderDate = new Date(this.dateValue);

            const orderYear = orderDate.getUTCFullYear();
            const orderMonth = this.prefixZero(orderDate.getUTCMonth() + 1);
            const orderDay = this.prefixZero(orderDate.getUTCDate());

            const queryDate = `${orderYear}-${orderMonth}-${orderDay}`;
            await this.navParams.data.modal.dismiss(queryDate);
        }
    }

    public async onModelConfirm() {
        if (this.dateValue) {
            const orderDate = new Date(this.dateValue);

            const orderYear = orderDate.getUTCFullYear();
            const orderMonth = this.prefixZero(orderDate.getUTCMonth() + 1);
            const orderDay = this.prefixZero(orderDate.getUTCDate());
            const timezoneOffset = orderDate.getTimezoneOffset() / 60;

            if (this.timeRange && this.startTime && this.endTime) {
                const startDate = new Date(this.startTime);
                const startHours = this.prefixZero(startDate.getHours());
                const startMinute = this.prefixZero(startDate.getMinutes());

                const startTime = `${orderYear}-${orderMonth}-${orderDay} ${startHours}:${startMinute}`;

                const endDate = new Date(this.endTime);
                const endHours = this.prefixZero(endDate.getHours());
                const endMinute = this.prefixZero(endDate.getMinutes());

                const endTime = `${orderYear}-${orderMonth}-${orderDay} ${endHours}:${endMinute}`;

                await this.navParams.data.modal.dismiss({
                    event: 'create_one',
                    startTime,
                    endTime,
                    timezoneOffset
                });
            }
        }
    }

    public async onBatchConfirm() {
        if (this.dateValue) {
            const newDate = new Date(this.dateValue);
            const orderYear = newDate.getUTCFullYear();
            const orderMonth = this.prefixZero(newDate.getUTCMonth() + 1);
            const orderDay = this.prefixZero(newDate.getUTCDate());
            const orderDate = `${orderYear}-${orderMonth}-${orderDay}`;
            const timezoneOffset = newDate.getTimezoneOffset() / 60;

            const resultData: any = {
                event: 'create_all',
                orderDate,
                timezoneOffset
            };

            if (this.startTime) {
                const startDate = new Date(this.startTime);
                const startHours = startDate.getHours();
                const startMinute = startDate.getMinutes();
                resultData.startTime = `${startHours}:${startMinute}`;
            }

            if (this.endTime) {
                const endDate = new Date(this.endTime);
                const endHours = endDate.getHours();
                const endMinute = endDate.getMinutes();
                resultData.endTime = `${endHours}:${endMinute}`;
            }

            await this.navParams.data.modal.dismiss(resultData);
        }
    }
}
