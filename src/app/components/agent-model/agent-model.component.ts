import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { HttpService } from 'src/app/service/http.service';

@Component({
    selector: 'app-agent-model',
    templateUrl: './agent-model.component.html',
    styleUrls: ['./agent-model.component.scss'],
})
export class AgentModelComponent implements OnInit {

    doctorPhone = null;
    doctorId = '';
    patientId = '';

    constructor(
        private modalController: ModalController,
        private navParams: NavParams,
        private httpService: HttpService,
    ) {
        this.doctorPhone = this.navParams.get('doctorPhone');
    }

    ngOnInit() {
    }

    public async onModelClose() {
        await this.modalController.dismiss();
    }

    public async onPhoneCall() {
        document.location.href = `tel:${this.doctorPhone}`;
    }

    public async onAgentClick() {
        const result: any = await this.httpService.post()
            .setPath('/order/createAgent')
            .setBody({
                doctorUserId: this.doctorId,
                patientUserId: this.patientId
            })
            .request();

        if (result.status === 0) {
            console.log(result);
        }
        await this.modalController.dismiss(true);
    }
}
