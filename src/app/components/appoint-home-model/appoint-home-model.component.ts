import { Component, OnInit } from '@angular/core';
import {ModalController, NavParams} from '@ionic/angular';
import {ThemeableBrowser, ThemeableBrowserOptions} from '@ionic-native/themeable-browser/ngx';
import {Device} from '@ionic-native/device/ngx';

@Component({
    selector: 'app-appointment-home-model',
    templateUrl: './appoint-home-model.component.html',
    styleUrls: ['./appoint-home-model.component.scss'],
})
export class AppointHomeModelComponent implements OnInit {

    title = null;
    cityAreaList = [];

    constructor(
        private device: Device,
        private modalController: ModalController,
        private navParams: NavParams,
        private themeableBrowser: ThemeableBrowser
    ) {
        this.title = navParams.get('title');
        this.cityAreaList = navParams.get('cityAreaList');
    }

    ngOnInit() {
    }

    public async onCityAreaClick(row = null) {
        if (row) {
            await this.modalController.dismiss({
                confirm: true,
                nationId: row.parent_id,
                cityAreaId: row.id
            });
        } else {
            await this.modalController.dismiss();
        }
    }

    public async onModelClose() {
        await this.modalController.dismiss();
    }

    public async onShowNotice() {
        const options: ThemeableBrowserOptions = {
            statusbar: {
                color: '#009e41'
            },
            toolbar: {
                height: 44,
                color: '#009e41'
            },
            title: {
                color: '#ffffff',
                staticText: '重要通知',
                showPageTitle: true
            },
            backButton: {
                wwwImage: '/assets/icon/direction-left.png',
                wwwImagePressed: '/assets/icon/direction-left.png',
                imagePressed: 'back_pressed',
                align: 'left',
                event: 'backPressed'
            },
            backButtonCanClose: true
        };

        this.themeableBrowser.create('http://www.sincaremedicaltour.com/news_detail.php?id=436', '_blank', options);
    }
}
