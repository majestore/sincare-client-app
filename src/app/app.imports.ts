import {BrowserModule} from '@angular/platform-browser';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {IonicStorageModule} from '@ionic/storage';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {AppRoutingModule} from './app-routing.module';

export function translateFunc(http: HttpClient) {
    // 国际化的文件地址
    return new TranslateHttpLoader(http, '../assets/i18n/', '.json');
}

export const Modules = [
    BrowserModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: translateFunc,
            deps: [HttpClient]
        }
    }),
    AppRoutingModule,
];

import { Device } from '@ionic-native/device/ngx';
import { Toast } from '@ionic-native/toast/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { NetworkInterface } from '@ionic-native/network-interface/ngx';
import { ThemeableBrowser } from '@ionic-native/themeable-browser/ngx';
import { HttpService } from './service/http.service';
import { BackService } from './service/back.service';
import { ToastService } from './service/toast.service';
import { WechatService } from './service/wechat.service';
import { TransferService } from './service/transfer.service';
import { FileService } from './service/file.service';

export const Providers = [
    HttpClientModule,
    Device,
    Toast,
    HTTP,
    File,
    FileTransfer,
    ImagePicker,
    FileChooser,
    NetworkInterface,
    ThemeableBrowser,
    HttpService,
    BackService,
    ToastService,
    WechatService,
    TransferService,
    FileService
];

import { DateFormatPipe } from './pipes/date-format.pipe';

export const Pipes = [
    DateFormatPipe
];
