import { NgModule } from '@angular/core';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { Modules, Providers } from './app.imports';

@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [
        Modules,
        IonicModule.forRoot({
            backButtonText: null
        }),
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
        Providers
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
