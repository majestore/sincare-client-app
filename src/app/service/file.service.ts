import { Injectable } from '@angular/core';
declare var fileChooser: any;
declare var FilePath: any;

@Injectable({
    providedIn: 'root'
})

export class FileService {

    constructor() {

    }

    public async openFileChooser(filter?: any) {
        return new Promise((resolve, reject) => {
            if (filter) {
                fileChooser.open(filter, resp => resolve(resp), error => reject(error));
            } else {
                fileChooser.open(uri => resolve(uri), error => reject(error));
            }
        });
    }

    public async resolveFileContent(content) {
        return new Promise((resolve, reject) => {
            FilePath.resolveNativePath(content, resp => resolve(resp), error => reject(error));
        });
    }
}
