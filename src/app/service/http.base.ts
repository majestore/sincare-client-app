export class HttpBase {
    public static EnvIndex = 2;

    private endpoint = [
        'http://localhost',
        'http://192.168.31.103',
        'http://39.100.112.123'
    ];
    private requestPort = 9210;

    constructor() {

    }

    getEndPoint(isNative) {
        return `${this.endpoint[HttpBase.EnvIndex]}:${this.requestPort}/api`;
    }
}
