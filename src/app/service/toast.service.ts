import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Device} from '@ionic-native/device/ngx';
import { Toast } from '@ionic-native/toast/ngx';

@Injectable({
    providedIn: 'root'
})

export class ToastService {

    public colorArray = ['care', 'secondary', 'tertiary', 'success', 'warning', 'danger', 'light', 'medium', 'dark'];
    private toastOption: any = {};

    constructor(public device: Device, private toastNtv: Toast, private toastCtrl: ToastController) {
        this.toastOption = {
            color: 'medium',
            duration: 3000,
            position: 'bottom',
            showCloseButton: false
        };
    }

    public setDuration(duration) {
        this.toastOption.duration = duration;
        return this;
    }

    public setColor(color) {
        this.toastOption.color = color;
        return this;
    }

    public setPosition(position) {
        if (this.device.platform) {
            this.toastOption.position = position;
        } else {
            this.toastOption.position = position === 'center' ? 'middle' : position;
        }
        return this;
    }

    public showToast(message = '') {
        if (this.device.platform) {
            this.toastNtv.show(message, this.toastOption.duration, this.toastOption.position)
                .subscribe(toast => console.log(toast));
        } else {
            this.toastCtrl.create({
                message,
                ...this.toastOption
            }).then(toast => toast.present());
        }

        this.toastOption = {
            color: 'medium',
            duration: 3000,
            position: 'bottom',
            showCloseButton: false
        };
    }
}
