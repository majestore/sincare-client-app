import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'guest-home',
        pathMatch: 'full'
    },
    {path: 'login', loadChildren: './pages/login/login.module#LoginPageModule'},
    {path: 'doctor-login', loadChildren: './pages/doctor/doctor-login/doctor-login.module#DoctorLoginPageModule'},
    {path: 'patient-login', loadChildren: './pages/patient/patient-login/patient-login.module#PatientLoginPageModule'},
    {path: 'patient-register', loadChildren: './pages/patient/patient-register/patient-register.module#PatientRegisterPageModule'},
    {path: 'doctor-register', loadChildren: './pages/doctor/doctor-register/doctor-register.module#DoctorRegisterPageModule'},
    {
        path: 'patient-login-success',
        loadChildren: './pages/patient/patient-login-success/patient-login-success.module#PatientLoginSuccessPageModule'
    },
    {
        path: 'doctor-login-success',
        loadChildren: './pages/doctor/doctor-login-success/doctor-login-success.module#DoctorLoginSuccessPageModule'
    },
    {path: 'doctor-password', loadChildren: './pages/doctor/doctor-password/doctor-password.module#DoctorPasswordPageModule'},
    {path: 'patient-password', loadChildren: './pages/patient/patient-password/patient-password.module#PatientPasswordPageModule'},
    {path: 'patient-app', loadChildren: './pages/patient/patient-app/patient-app.module#PatientAppPageModule'},
    {path: 'doctor-app', loadChildren: './pages/doctor/doctor-app/doctor-app.module#DoctorAppPageModule'},
    {path: 'test', loadChildren: './pages/test/test.module#TestPageModule'},
    {path: 'settings', loadChildren: './pages/settings/settings.module#SettingsPageModule'},
    {
        path: 'patient-person-edit',
        loadChildren: './pages/patient/patient-person-edit/patient-person-edit.module#PatientPersonEditPageModule'
    },
    {
        path: 'doctor-personal-center',
        loadChildren: './pages/doctor/doctor-personal-center/doctor-personal-center.module#DoctorPersonalCenterPageModule'
    },
    {path: 'doctor-person-edit', loadChildren: './pages/doctor/doctor-person-edit/doctor-person-edit.module#DoctorPersonEditPageModule'},
    {
        path: 'patient-personal-center',
        loadChildren: './pages/patient/patient-personal-center/patient-personal-center.module#PatientPersonalCenterPageModule'
    },
    {path: 'patient-doctorinfo', loadChildren: './pages/patient/patient-doctorinfo/patient-doctorinfo.module#PatientDoctorinfoPageModule'},
    {
        path: 'patient-registion-times',
        loadChildren: './pages/patient/patient-registion-times/patient-registion-times.module#PatientRegistionTimesPageModule'
    },

    {
        path: 'doctor-appoint-details',
        loadChildren: './pages/doctor/doctor-appoint-details/doctor-appoint-details.module#DoctorAppointDetailsPageModule'
    },
    {
        path: 'patient-appoint-details',
        loadChildren: './pages/patient/patient-appoint-details/patient-appoint-details.module#PatientAppointDetailsPageModule'
    },
    { path: 'doctor-my-messages', loadChildren: './pages/doctor/doctor-my-messages/doctor-my-messages.module#DoctorMyMessagesPageModule'},
    { path: 'guest-home', loadChildren: './pages/guest/guest-home/guest.module#GuestPageModule' },
    { path: 'guest-doctorinfo', loadChildren: './pages/guest/guest-doctorinfo/guest-doctorinfo.module#GuestDoctorinfoPageModule' },
    { path: 'privacy', loadChildren: './pages/privacy/privacy/privacy.module#PrivacyPageModule' },
    { path: 'hospital-home', loadChildren: './pages/hospital/hospital-home/hospital.module#HospitalPageModule' },
    { path: 'hospital-info', loadChildren: './pages/hospital/hospital-info/hospital-info.module#HospitalInfoPageModule' },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
