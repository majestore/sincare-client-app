import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateFormat'
})
export class DateFormatPipe implements PipeTransform {

  prefixZero(num, n = 2) {
    return (Array(n).join('0') + num).slice(-n);
  }

  transform(value: any, args?: any): any {
    const dateTimestamp = new Date(value);
    const year = dateTimestamp.getFullYear();
    const month = this.prefixZero(dateTimestamp.getMonth() + 1);
    const day = this.prefixZero(dateTimestamp.getDate());
    const hours = this.prefixZero(dateTimestamp.getHours());
    const minute = this.prefixZero(dateTimestamp.getMinutes());
    return `${year}-${month}-${day} ${hours}:${minute}`;
  }

}
