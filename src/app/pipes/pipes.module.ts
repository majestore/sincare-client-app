import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateFormatPipe } from './date-format.pipe';

@NgModule({
    imports: [
        CommonModule
    ],
    exports: [
        DateFormatPipe
    ],
    declarations: [
        DateFormatPipe
    ]
})
export class PipeModuleModule {
    static forRoot() {
        return {
            ngModule: PipeModuleModule,
            providers: [],
        };
    }
}
