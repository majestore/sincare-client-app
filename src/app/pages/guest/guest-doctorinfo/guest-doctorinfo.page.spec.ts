import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestDoctorinfoPage } from './guest-doctorinfo.page';

describe('GuestDoctorinfoPage', () => {
  let component: GuestDoctorinfoPage;
  let fixture: ComponentFixture<GuestDoctorinfoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestDoctorinfoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestDoctorinfoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
