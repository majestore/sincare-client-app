import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from 'src/app/service/http.service';
import { AlertController, ModalController, NavController } from '@ionic/angular';
import { AgentModelComponent } from '../../../components/agent-model/agent-model.component';
import { ServiceModelComponent } from 'src/app/components/service-model/service-model.component';
import { ImgModalComponent } from 'src/app/components/img-modal/img-modal.component';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-guest-doctorinfo',
  templateUrl: './guest-doctorinfo.page.html',
  styleUrls: ['./guest-doctorinfo.page.scss'],
})
export class GuestDoctorinfoPage implements OnInit {
  doctorId = 0;
  doctorInfo: any = {};
  hospitalName = '';
  sysLang = '';
  constructor(
    private activatedRoute: ActivatedRoute,
    private httpService: HttpService,
    public alertController: AlertController,
    private router: Router,
    private modalController: ModalController,
    private translate: TranslateService,
    private navController: NavController,
  ) {
    activatedRoute.queryParams.subscribe(queryParams => {
      if (!!queryParams.doctorId) {
        this.doctorId = queryParams.doctorId;
        console.log(this.doctorId);
        this.getDoctorinfo();
      }
    });
  }
  ngOnInit() {
    this.sysLang = this.translate.getBrowserCultureLang().toLowerCase();
  }

  private async getDoctorinfo() {
    const self = this;
    const result: any = await self.httpService.get()
      .setPath('/pub/user/doctorInfo/' + self.doctorId)
      .request();
    if (result.status === 0) {
      this.doctorInfo = result.data;
      this.hospitalName = this.doctorInfo.hospitalNameArray[0].hospital_name;
    }
  }

  public async toNext() {
    let aHeader = '';
    let aMessage = '';
    let aCancel = '';
    let aOk = '';
    if (this.sysLang === 'en-us') {
      aHeader = 'Alert';
      aMessage = 'Login/Register to experience more';
      aCancel = 'Cancel';
      aOk = 'Next';
    } else {
      aHeader = '提示';
      aMessage = '登录/注册后方可体验更多功能';
      aCancel = '取消';
      aOk = '立即前往';
    }
    if (this.doctorInfo.order_type === 1) {
      this.router.navigate(['patient-registion-times'], {
        queryParams: { doctorId: this.doctorInfo.id }
      });
    } else {
      const alert = await this.alertController.create({
        header: aHeader,
        message: aMessage,
        buttons: [
          {
            text: aCancel,
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: aOk,
            handler: () => {
              this.navController.navigateForward(['login'], {
                queryParams: {}
              });
            }
          }
        ]
      });
      await alert.present();
    }
  }

  public async presentAlert() {
    const aMessage = this.sysLang === 'en-us' ? 'Please complete your personal info' : '请完善个人信息之后挂号';
    const aYes = this.sysLang === 'en-us' ? 'Confirm' : '确定';
    const aNo = this.sysLang === 'en-us' ? 'Cancel' : '取消';
    const alert = await this.alertController.create({
      message: aMessage,
      buttons: [
        {
          text: aNo,
          role: 'cancel',
          handler: () => { }
        },
        {
          text: aYes,
          role: '',
          handler: () => {
            this.router.navigate(['patient-personal-center']);
          }
        }
      ]
    });
    await alert.present();
  }

  public async onPopoverAgency() {
    const model = await this.modalController.create({
      component: ServiceModelComponent,
      // cssClass: 'service-model'
    });
    await model.present();
    const { data } = await model.onDidDismiss();
  }

  public async showCH() {
    const modal = await this.modalController.create({
      component: ImgModalComponent,
      componentProps: {
        title: 'CooperativeHospitals',
        hospitalArr: this.doctorInfo.hospitalNameArray
      }
    });
    await modal.present();
  }

  public async showPI() {
    const modal = await this.modalController.create({
      component: ImgModalComponent,
      componentProps: {
        title: 'PersonInfo',
        personalInfo: this.doctorInfo.personal_desc
      }
    });
    await modal.present();
  }

  public async showMQ() {
    const modal = await this.modalController.create({
      component: ImgModalComponent,
      componentProps: {
        title: 'MedicalQualifications',
        imgId: this.doctorInfo.attachment_url
      }
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
  }

  public async showPH() {
    const modal = await this.modalController.create({
      component: ImgModalComponent,
      componentProps: {
        title: 'PersonalHonors',
        personalRewards: this.doctorInfo.personal_honor
      }
    });
    await modal.present();
  }
}
