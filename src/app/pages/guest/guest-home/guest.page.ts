import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, PopoverController, AlertController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { HttpService } from 'src/app/service/http.service';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Device } from '@ionic-native/device/ngx';
import { RegionPickModelComponent } from '../../../components/region-pick-model/region-pick-model.component';
import { RadioPickModelComponent } from '../../../components/radio-pick-model/radio-pick-model.component';
import { CalendarModelComponent } from '../../../components/calendar-model/calendar-model.component';
import { AppointHomeModelComponent } from '../../../components/appoint-home-model/appoint-home-model.component';
import {ThemeableBrowser, ThemeableBrowserOptions} from '@ionic-native/themeable-browser/ngx';

@Component({
  selector: 'app-guest',
  templateUrl: './guest.page.html',
  styleUrls: ['./guest.page.scss'],
})
export class GuestPage implements OnInit {
  isAgree = false;
  doctorList = [];
  stopLoad = false;
  checkedList: Array<any> = [];
  departmentId = '';
  hospitalId = '';
  regionId = '';
  queryParams: any = {};
  queryOffset = 0;
  queryLimit = 10;
  menuHeaderStyle = {};
  isIos = 0; // 0不是ios 1是ios
  sysLang = '';
  launcherType = 1;
  cityAreaList = [];

  segmentTabs: Array<any> = [
    {
      label: 'Region',
      value: 'region'
    },
    {
      label: 'Department',
      value: 'department'
    },
    {
      label: 'Date',
      value: 'appoint'
    },
  ];

  constructor(
      private device: Device,
      private router: Router,
      private statusBar: StatusBar,
      private navController: NavController,
      private httpService: HttpService,
      private translate: TranslateService,
      private modalController: ModalController,
      private alertController: AlertController,
      private toastController: ToastController,
      private themeableBrowser: ThemeableBrowser,
  ) { }

  async ngOnInit() {
    await this.getLauncherType();
    await this.queryDoctorList(true);

    this.statusBar.backgroundColorByHexString('#009e41');
    if (this.device.platform) {
      if (this.device.platform.toUpperCase() === 'ANDROID') {
        this.menuHeaderStyle = { 'margin-top': '44px' };
        this.isIos = 0;
      } else if (this.device.platform.toUpperCase() === 'IOS') {
        this.menuHeaderStyle = { 'margin-top': '44px' };
        this.isIos = 1;
      }
    }

    /*if (this.launcherType === 2) {
      const userId = localStorage.getItem('userId');
      if (!userId) {
        await this.getCityAreaList();
        await this.showAppointmentModel();
      }
    }*/
  }

  public async onSegmentClick(value) {
    switch (value) {
      case 'region':
        await this.showRegionModal();
        break;

      case 'department':
        await this.showDepartmentModel();
        break;

      case 'appoint':
        await this.showCalendarModel();
        break;
    }

    await this.queryDoctorList(true);
  }

  public async showCalendarModel() {
    const modal = await this.modalController.create({
      component: CalendarModelComponent,
      componentProps: {
        title: 'SelectDate',
      }
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if (!!data) {
      this.queryParams.queryDate = data;
    }
  }

  public async onAppointmentHospital() {
    await this.navController.navigateForward(['hospital-home']);
  }

  private async getCityAreaList() {
    const result = await this.httpService.get()
        .setPath('/pub/source/cityAreaList')
        .request();

    if (result.status === 0) {
      this.cityAreaList = result.data;
    }
  }

  private async showAppointmentModel() {
    const modal = await this.modalController.create({
      component: AppointHomeModelComponent,
      componentProps: {
        cityAreaList: this.cityAreaList
      }
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if (data) {
      this.queryParams = {
        ...this.queryParams,
        ...data
      };
      await this.queryDoctorList(true);
    } else {
      if (this.launcherType !== 2) {
        await this.showAlertPloicy();
      }
    }
  }

  public onHelp() {
    let helpTitle = '使用帮助';
    if (this.sysLang === 'en-us') {
      helpTitle = 'Guide';
    }

    const helpLink = 'http://www.sincaremedicaltour.com/news_detail.php?id=438';
    const options: ThemeableBrowserOptions = {
      statusbar: {
        color: '#009e41'
      },
      toolbar: {
        height: 44,
        color: '#009e41'
      },
      title: {
        color: '#ffffff',
        staticText: helpTitle,
        showPageTitle: true
      },
      backButton: {
        wwwImage: '/assets/icon/direction-left.png',
        wwwImagePressed: '/assets/icon/direction-left.png',
        imagePressed: 'back_pressed',
        align: 'left',
        event: 'backPressed'
      },
      backButtonCanClose: true
    };

    this.themeableBrowser.create(helpLink, '_blank', options);
  }

  private async getLauncherType() {
    const result = await this.httpService.get()
        .setPath('/pub/user/getLauncherType')
        .request();

    if (result.status === 0) {
      this.launcherType = Number(result.data.launcherType);
      if (this.launcherType === 2) {
        this.segmentTabs = this.segmentTabs.filter(item => item.value !== 'department');
      }
    }
  }

  public async queryDoctorList(refresh: boolean) {
    if (refresh) {
      this.queryOffset = 0;
    }

    const requestMethod = {
      1: '/pub/user/doctorList',
      2: '/pub/user/hospitalUserList'
    };

    if (!this.queryParams.queryDate) {
      // this.queryParams.queryDate = this.httpService.getDateString();
      this.queryParams.queryDate = this.httpService.getTomorrowDateString();
    }

    const result = await this.httpService.get()
        .setPath(requestMethod[this.launcherType])
        .setQuery({
          ...this.queryParams,
          offset: this.queryOffset,
          limit: this.queryLimit,
          doctorState: 1,
        })
        .request();

    if (result.status === 0) {
      if (refresh) {
        this.doctorList = result.data.rows;
      } else {
        if (this.doctorList.length < result.data.count) {
          this.doctorList = [
            ...this.doctorList,
            ...result.data.rows
          ];
        }
      }
      this.queryOffset = this.doctorList.length;
      this.stopLoad = this.doctorList.length === result.data.count;
    }
  }

  public async onDoctorSelect(doctor) {
    if (!!this.isAgree || this.launcherType === 2) {
      if (this.launcherType === 2) {
        if (doctor.order_type === 1) {
          if (doctor.has_order) {
            await this.navController.navigateForward(['hospital-info'], {
              queryParams: {
                hospitalUserId: doctor.id,
                hasOrder: doctor.has_order
              }
            });
          } else {
            this.onHelp();
          }
        } else if (doctor.order_type === 2) {
          this.onHelp();
        }
      } else {
        await this.navController.navigateForward(['guest-doctorinfo'], {
          queryParams: {
            doctorId: doctor.id,
            hasOrder: doctor.has_order
          }
        });
      }
    } else {
      await this.showAlertPloicy();
    }
  }

  public async doRefresh(event) {
    await this.queryDoctorList(true);
    event.target.complete();
  }

  public async loadData(event) {
    await this.queryDoctorList(false);
    event.target.complete();
  }

  private async showRegionModal() {
    const result: any = await this.httpService.get()
        .setPath('/pub/source/threeRegionData')
        .setQuery({
          style: 'tree',
          appointNation: this.launcherType === 2 ? 'us' : null,
          isCityArea: this.launcherType === 2 ? 1 : null
        })
        .request();

    if (result.status === 0) {
      const modal = await this.modalController.create({
        component: RegionPickModelComponent,
        componentProps: {
          title: 'District Choose',
          fixedNation: this.launcherType === 2 ? 'us' : null,
          itemArray: [
            {
              name: 'Country Choose',
              type: 1,
            },
            {
              name: 'State Choose',
              type: 2,
            },
            {
              name: 'City Choose',
              type: 3
            },
          ],
          dataArray: [...result.data]
        }
      });
      await modal.present();
      const { data } = await modal.onDidDismiss();
      if (data) {
        const [, , region] = data;
        delete this.queryParams.cityAreaId;
        this.queryParams.regionId = region.value;
      }
    }
  }

  private async showDepartmentModel() {
    const result: any = await this.httpService.get()
        .setPath('/pub/source/departmentList')
        .request();

    if (result.status === 0) {
      const modal = await this.modalController.create({
        component: RadioPickModelComponent,
        componentProps: {
          title: '选择部门',
          labelProp: 'name',
          dataList: [...result.data],
          selectValue: this.queryParams.departmentId
        }
      });
      await modal.present();
      const { data } = await modal.onDidDismiss();
      if (data) {
        this.queryParams.departmentId = data.value;
      }
    }
  }

  async onFilterReset() {
    this.queryParams = {};
    await this.queryDoctorList(true);
  }

  async toLogin() {
    if (!!this.isAgree || this.launcherType === 2) {
      await this.navController.navigateForward(['login'], {
        queryParams: {}
      });
    } else {
      await this.showAlertPloicy();
    }
  }

  async showAlertPloicy() {
    // tslint:disable-next-line: max-line-length
    const aCHN = '<p>1.特别提示</p><p>1.1为了更好地为您提供服务，请您仔细阅读这份协议。本协议是您与本应用就您登录本应用平台进行注册及使用等所涉及的全部行为所订立的权利义务规范。您在注册过程中点击&ldquo;注册&rdquo;等按钮、及注册后登录和使用时，均表明您已完全同意并接受本协议，愿意遵守本协议的各项规则、规范的全部内容，若不同意则可停止注册或使用本应用平台。如您是未成年人，您还应要求您的监护人仔细阅读本协议，并取得他/他们的同意。</p><p>1.2为提高用户的使用感受和满意度，用户同意本应用将基于用户的操作行为对用户数据进行调查研究和分析，从而进一步优化服务。</p><p>2.服务内容</p><p>2.1信挂号APP是一个真实医疗挂号平台，平台内严禁一切非法信息、恶意侵占医疗资源等行为，如发现，则作封号处理。</p><p>2.2本应用服务的具体内容由本应用制作者根据实际情况提供。</p><p>&nbsp;2.3 除非本注册及服务协议另有其它明示规定，本应用所推出的新产品、新功能、新服务，均受到本注册及注册协议规范。</p><p>2.4 本应用仅提供相关的网络服务，除此之外与相关网络服务有关的设备(如个人电脑、手机、及其他与接入互联网或移动网有关的装置)及所需的费用(如为接入互联网而支付的电话费及上网费、为使用移动网而支付的手机费)均应由用户自行负担。</p><p>3.使用规则</p><p>3.1 用户帐号注册</p><p>3.1.1使用本应用系统注册的用户，只能使用汉字、英文字母、数字、下划线及它们的组合，禁止使用空格、各种符号和特殊字符，且最多不超过16个字符(8个汉字)注册，否则将不予注册。</p><p>3.1.2使用第三方合作网站登录的用户，只能使用汉字、英文字母、数字、下划线及它们的组合，禁止使用空格、各种符号和特殊字符，且最多不超过14个字符(7个汉字)注册，否则本社区有权只截取前14个字符（7个汉字）予以显示用户帐号（若该用户帐号与应用现有用户帐号重名，系统将随机添加一个字符以示区别），否则将不予注册。</p><p>3.2如发现用户帐号信息中含有不雅文字或有害信息影响正常系统管理的，信挂号保留取消其用户资格的权利。</p><p>3.3用户帐号的所有权归本应用，用户仅享有使用权。</p><p>3.4用户有义务保证密码和帐号的安全，用户利用该密码和帐号所进行的一切活动引起的任何损失或损害，由用户自行承担全部责任，本应用不承担任何责任。如用户发现帐号遭到未授权的使用或发生其他任何安全问题，应立即修改帐号密码并妥善保管，如有必要，请反馈通知本应用管理人员。因黑客行为或用户的保管疏忽导致帐号非法使用，本应用不承担任何责任。</p><p>3.5用户承诺对其发表或者上传于本应用的所有信息(即属于《中华人民共和国著作权法》规定的作品，包括但不限于文字、图片、音乐、电影、表演和录音录像制品和电脑程序等)均享有完整的知识产权，或者已经得到相关权利人的合法授权；如用户违反本条规定造成本应用被第三人索赔的，用户应全额补偿本应用的一切费用(包括但不限于各种赔偿费、诉讼代理费及为此支出的其它合理费用)；</p><p>3.6当第三方认为用户发表或者上传于本应用的信息侵犯其权利，并根据《信息网络传播权保护条例》或者相关法律规定向本应用发送权利通知书时，用户同意本应用可以自行判断决定删除涉嫌侵权信息，除非用户提交书面证据材料排除侵权的可能性，本应用将不会自动恢复上述删除的信息；</p><p>(1)不得为任何非法目的而使用网络服务系统；</p><p>(2)遵守所有与网络服务有关的网络协议、规定和程序；</p><p>(3)不得利用本应用的服务进行任何可能对互联网的正常运转造成不利影响的行为；</p><p>(4)不得利用本应用服务进行任何不利于本应用的行为。</p><p>3.7如用户在使用网络服务时违反上述任何规定，本应用有权要求用户改正或直接采取一切必要的措施(包括但不限于删除用户上传的内容、暂停或终止用户使用网络服务的权利)以减轻用户不当行为而造成的影响。</p><p>4.责任声明</p><p>4.1任何网站、单位或者个人如认为本应用或者本应用提供的相关内容涉嫌侵犯其合法权益，应及时向本应用提供书面权力通知，并提供身份证明、权属证明及详细侵权情况证明。本应用在收到上述法律文件后，将会尽快切断相关内容以保证相关网站、单位或者个人的合法权益得到保障。</p><p>4.2用户明确同意其使用本应用网络服务所存在的风险及一切后果将完全由用户本人承担，本应用对此不承担任何责任。</p><p>4.3本应用无法保证网络服务一定能满足用户的要求，也不保证网络服务的及时性、安全性、准确性。</p><p>4.4本应用不保证为方便用户而设置的外部链接的准确性和完整性，同时，对于该等外部链接指向的不由本应用实际控制的任何网页上的内容，本应用不承担任何责任。</p><p>5.知识产权</p><p>5.1 本应用特有的标识、版面设计、编排方式等版权均属本应用享有，未经本应用许可授权，不得任意复制或转载。</p><p>5.2 用户从本应用的服务中获得的信息，未经本应用的许可，不得任意复制或转载。</p><p>5.3 本应用的所有内容，包括商品描述、图片等内容所有权归属于心动APP的用户，任何人不得转载。</p><p>5.4 本应用所有用户上传内容仅代表用户自己的立场和观点，与本应用无关，由作者本人承担一切法律责任。</p><p>5.5上述及其他任何本服务包含的内容的知识产权均受到法律保护，未经本应用、用户或相关权利人书面许可，任何人不得以任何形式进行使用或创造相关衍生作品。</p><p>6.隐私保护</p><p>6.1 本应用不对外公开或向第三方提供单个用户的注册资料及用户在使用网络服务时存储在App的非公开内容，但下列情况除外：</p><p>(1)事先获得用户的明确授权；</p><p>(2)根据有关的法律法规要求；</p><p>(3)按照相关政府主管部门的要求；</p><p>(4)为维护社会公众的利益。</p><p>6.2 由于本App是挂号平台，本应用可能会与第三方合作向用户提供相关的网络服务，在此情况下，如该第三方同意承担与本App同等的保护用户隐私的责任，则本App有权将用户的注册资料等信息提供给该第三方，并无须另行告知用户。</p><p>6.3 在不透露单个用户隐私资料的前提下，本应用有权对整个用户数据库进行分析并对用户数据库进行商业上的利用。</p><p>7.协议修改</p><p>7.1本应用有权随时修改本协议的任何条款，一旦本协议的内容发生变动，本应用将会在本应用上公布修改之后的协议内容，若用户不同意上述修改，则可以选择停止使用本应用。本应用也可选择通过其他适当方式（比如系统通知）向用户通知修改内容。</p><p>7.2如果不同意本应用对本协议相关条款所做的修改，用户有权停止使用本应用。如果用户继续使用本应用，则视为用户接受本应用对本协议相关条款所做的修改。</p><p>8.通知送达</p><p>8.1本协议项下本应用对于用户所有的通知均可通过网页公告、电子邮件、系统通知、微博管理帐号主动联系、私信、手机短信或常规的信件传送等方式进行；该等通知于发送之日视为已送达收件人。</p><p>8.2用户对于本应用的通知应当通过本应用对外正式公布的通信地址、电子邮件地址等联系信息进行送达。</p>';
    // tslint:disable-next-line: max-line-length
    const aENU = '<p class="MsoNormal" style="text-align: left; line-height: 36.0pt; mso-pagination: widow-orphan; mso-outline-level: 2; background: white; margin: 18.0pt 0cm 18.0pt 0cm;" align="left"><span lang="EN-US" style="font-size: 34.0pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">Privacy Policy</span></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">Sincare Medical Tour Concierge built the EasyDoc app as a Commercial app. This SERVICE is provided by Sincare Medical Tour Concierge and is intended for use as is.</span></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">This page is used to inform visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.</span></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">If you choose to use our Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that we collect is used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.</span></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at EasyDoc unless otherwise defined in this Privacy Policy.</span></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><strong><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">Information Collection and Use</span></strong></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">For a better experience, while using our Service, we may require you to provide us with certain personally identifiable information, including but not limited to Basic personal information (such as name, age, etc.) and medical records.. The information that we request will be retained by us and used as described in this privacy policy.</span></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">The app does use third party services that may collect information used to identify you.</span></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">Link to privacy policy of third party service providers used by the app</span></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><span lang="EN-US"><span style="mso-tab-count: 1;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>WeChat Open Services</span>：<span lang="EN-US"><a href="https://open.weixin.qq.com/cgi-bin/frame?t=news/protocol_developer_tmpl">https://open.weixin.qq.com/cgi-bin/frame?t=news/protocol_developer_tmpl</a></span></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><strong><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">Log Data</span></strong></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">We want to inform you that whenever you use our Service, in a case of an error in the app we collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (&ldquo;IP&rdquo;) address, device name, operating system version, the configuration of the app when utilizing our Service, the time and date of your use of the Service, and other statistics.</span></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><strong><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">Cookies</span></strong></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device‘s internal memory.</span></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">This Service does not use these &ldquo;cookies&rdquo; explicitly. However, the app may use third party code and libraries that use &ldquo;cookies&rdquo; to collect information and improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this Service.</span></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><strong><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">Service Providers</span></strong></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">We may employ third-party companies and individuals due to the following reasons:</span></p><ul type="disc"><li class="MsoNormal" style="color: #616161; mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; mso-list: l0 level1 lfo1; tab-stops: list 36.0pt; background: white;"><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; mso-font-kerning: 0pt;">To facilitate our Service;</span></li><li class="MsoNormal" style="color: #616161; mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; mso-list: l0 level1 lfo1; tab-stops: list 36.0pt; background: white;"><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; mso-font-kerning: 0pt;">To provide the Service on our behalf;</span></li><li class="MsoNormal" style="color: #616161; mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; mso-list: l0 level1 lfo1; tab-stops: list 36.0pt; background: white;"><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; mso-font-kerning: 0pt;">To perform Service-related services; or</span></li><li class="MsoNormal" style="color: #616161; mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; mso-list: l0 level1 lfo1; tab-stops: list 36.0pt; background: white;"><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; mso-font-kerning: 0pt;">To assist us in analyzing how our Service is used.</span></li></ul><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">We want to inform users of this Service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.</span></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><strong><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">Security</span></strong></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.</span></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><strong><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">Links to Other Sites</span></strong></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by us. Therefore, we strongly advise you to review the Privacy Policy of these websites. We have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.</span></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><strong><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">Children&rsquo;s Privacy</span></strong></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">These Services do not address anyone under the age of 13. We do not knowingly collect personally identifiable information from children under 13. In the case we discover that a child under 13 has provided us with personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to do necessary actions.</span></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><strong><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">Changes to This Privacy Policy</span></strong></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">We may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately after they are posted on this page.</span></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><strong><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">Contact Us</span></strong></p><p class="MsoNormal" style="margin-bottom: 12.0pt; text-align: left; line-height: 18.0pt; mso-pagination: widow-orphan; background: white;" align="left"><span lang="EN-US" style="mso-bidi-font-size: 10.5pt; font-family: Helvetica; mso-fareast-font-family: 宋体; mso-bidi-font-family: Helvetica; color: #616161; mso-font-kerning: 0pt;">If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us at sincare2010@gmail.com</span></p><p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>';
    const aheader = this.sysLang === 'en-us' ? 'Privacy Policy of EasyDoc App' : '用户使用与隐私协议';
    const aMessage = this.sysLang === 'en-us' ? aENU : aCHN;
    const disagreeText = this.sysLang === 'en-us' ? 'Disagree' : '不同意';
    const agreeText = this.sysLang === 'en-us' ? 'Agree' : '同意';
    const alert = await this.alertController.create({
      header: aheader,
      message: aMessage,
      backdropDismiss: false,
      buttons: [
        {
          text: disagreeText,
          handler: () => {
            // tslint:disable-next-line: max-line-length
            const aToast = this.sysLang === 'en-us' ? 'If you choose to use our Service, then you need agree to the collection and use of information in relation to this privacy policy.' : '您需同意信凯尔用户使用与隐私协议方可使用本软件';
            this.presentToast(aToast);
          }},
        {
          text: agreeText,
          handler: () => {
            this.isAgree = true;
            this.navController.navigateForward(['login'], {
              queryParams: {}
            });
          }
        }
      ]
    });
    await alert.present();
  }

  async presentToast(words) {
    const toast = await this.toastController.create({
      message: words,
      duration: 2000
    });
    toast.present();
  }
}
