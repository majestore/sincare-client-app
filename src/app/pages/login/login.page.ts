import {Component, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';
import {HttpBase} from '../../service/http.base';
import {HttpService} from '../../service/http.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    bootOptions = {};

    constructor(private navCtrl: NavController, private httpService: HttpService) {
    }

    async ngOnInit() {

    }

    async ionViewDidEnter() {
        await this.getBootOptions();
    }

    async getBootOptions() {
        const result: any = await this.httpService.get()
            .setPath('/pub/user/getBootOptions')
            .request();
        if (result.status === 0) {
            this.bootOptions = result.data;
        }
    }

    onUserLogin() {
        this.navCtrl.navigateRoot('/doctor-home');
        localStorage.setItem('login', 'login');
    }

    toPatient() {
        this.navCtrl.navigateForward('/patient-login', {
            queryParams: this.bootOptions
        });
    }

    toDoctor() {
        this.navCtrl.navigateForward('/doctor-login');
    }

    onEntryTest() {
        if (HttpBase.EnvIndex < 2) {
            this.navCtrl.navigateForward('/test');
        }
    }
}
