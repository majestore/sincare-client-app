import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HospitalPage } from './hospital.page';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from '../../../components';

const routes: Routes = [
  {
    path: '',
    component: HospitalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule,
    ComponentsModule
  ],
  declarations: [HospitalPage]
})
export class HospitalPageModule {}
