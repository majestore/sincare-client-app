import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import {ActivatedRoute, Router} from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { HttpService } from 'src/app/service/http.service';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Device } from '@ionic-native/device/ngx';
import { RegionPickModelComponent } from '../../../components/region-pick-model/region-pick-model.component';
import { CalendarModelComponent } from '../../../components/calendar-model/calendar-model.component';

@Component({
  selector: 'app-hospital-page',
  templateUrl: './hospital.page.html',
  styleUrls: ['./hospital.page.scss'],
})
export class HospitalPage implements OnInit {
  hospitalUserList = [];
  stopLoad = false;
  checkedList: Array<any> = [];
  departmentId = '';
  hospitalId = '';
  regionId = '';
  queryParams: any = {};
  queryOffset = 0;
  queryLimit = 10;
  menuHeaderStyle = {};
  isIos = 0; // 0不是ios 1是ios
  sysLang = '';

  segmentTabs: Array<any> = [
    {
      label: 'Region',
      value: 'region'
    },
    {
      label: 'Date',
      value: 'appoint'
    },
  ];

  constructor(
    private device: Device,
    private router: Router,
    private statusBar: StatusBar,
    private navController: NavController,
    private httpService: HttpService,
    private translate: TranslateService,
    private modalController: ModalController,
    private activatedRoute: ActivatedRoute
  ) {
    activatedRoute.queryParams.subscribe(async queryParams => {
      if (queryParams.cityAreaId) {
        this.queryParams.cityAreaId = queryParams.cityAreaId;
      }

      await this.queryHospitalUserList(true);
    });
  }

  async ngOnInit() {
    this.statusBar.backgroundColorByHexString('#009e41');
    if (this.device.platform) {
      if (this.device.platform.toUpperCase() === 'ANDROID') {
        this.menuHeaderStyle = { 'margin-top': '44px' };
        this.isIos = 0;
      } else if (this.device.platform.toUpperCase() === 'IOS') {
        this.menuHeaderStyle = { 'margin-top': '44px' };
        this.isIos = 1;
      }
    }
  }

  public async onSegmentClick(value) {
    switch (value) {
      case 'region':
        await this.showRegionModal();
        break;

      case 'appoint':
        await this.showCalendarModel();
        break;
    }

    await this.queryHospitalUserList(true);
  }

  public async showCalendarModel() {
    const modal = await this.modalController.create({
      component: CalendarModelComponent,
      componentProps: {
        title: 'SelectDate',
      }
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if (!!data) {
      this.queryParams.queryDate = data;
    }
  }

  public async queryHospitalUserList(refresh: boolean) {
    if (refresh) {
      this.queryOffset = 0;
    }

    const result = await this.httpService.get()
      .setPath('/pub/user/hospitalUserList')
      .setQuery({
        ...this.queryParams,
        offset: this.queryOffset,
        limit: this.queryLimit,
        hospitalUserState: 1,
      })
      .request();

    if (result.status === 0) {
      if (refresh) {
        this.hospitalUserList = result.data.rows;
      } else {
        if (this.hospitalUserList.length < result.data.count) {
          this.hospitalUserList = [
            ...this.hospitalUserList,
            ...result.data.rows
          ];
        }
      }
      this.queryOffset = this.hospitalUserList.length;
      this.stopLoad = this.hospitalUserList.length === result.data.count;
    }
  }

  public async onHospitalUserSelect(hospitalUser) {
    await this.navController.navigateForward(['hospital-info'], {
      queryParams: {
        hospitalUserId: hospitalUser.id
      }
    });
  }

  public async doRefresh(event) {
    await this.queryHospitalUserList(true);
    event.target.complete();
  }

  public async loadData(event) {
    await this.queryHospitalUserList(false);
    event.target.complete();
  }

  private async showRegionModal() {
    const result: any = await this.httpService.get()
      .setPath('/pub/source/threeRegionData')
      .setQuery({
        style: 'tree',
        appointNation: 'us'
      })
      .request();

    if (result.status === 0) {
      const modal = await this.modalController.create({
        component: RegionPickModelComponent,
        componentProps: {
          title: 'District Choose',
          fixedNation: 'us',
          itemArray: [
            {
              name: 'Country Choose',
              type: 1,
            },
            {
              name: 'State Choose',
              type: 2,
            },
            {
              name: 'City Choose',
              type: 3
            },
          ],
          dataArray: [...result.data]
        }
      });
      await modal.present();
      const { data } = await modal.onDidDismiss();
      if (data) {
        const [, , region] = data;
        delete this.queryParams.cityAreaId;
        this.queryParams.regionId = region.value;
      }
    }
  }

  async onFilterReset() {
    this.queryParams = {};
    await this.queryHospitalUserList(true);
  }
}
