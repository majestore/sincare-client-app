import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {PatientAppPage} from './patient-app.page';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'patient-home',
        pathMatch: 'full'
    },
    {
        path: 'patient-home',
        loadChildren: '../patient-home/patient-home.module#PatientHomePageModule'
    },
    {
        path: 'patient-personal-center',
        loadChildren: '../patient-personal-center/patient-personal-center.module#PatientPersonalCenterPageModule'
    },

    {
        path: 'patient-my-appointments',
        loadChildren: '../patient-my-appointments/patient-my-appointments.module#PatientMyAppointmentsPageModule'
    },
    {
        path: 'patient-my-messages',
        loadChildren: '../patient-my-messages/patient-my-messages.module#PatientMyMessagesPageModule'
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [PatientAppPage]
})
export class PatientAppPageModule {
}
