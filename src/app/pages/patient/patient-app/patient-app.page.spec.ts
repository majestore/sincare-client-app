import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientAppPage } from './patient-app.page';

describe('PatientAppPage', () => {
  let component: PatientAppPage;
  let fixture: ComponentFixture<PatientAppPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientAppPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientAppPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
