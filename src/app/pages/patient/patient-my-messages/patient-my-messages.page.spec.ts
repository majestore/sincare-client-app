import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientMyMessagesPage } from './patient-my-messages.page';

describe('PatientMyMessagesPage', () => {
  let component: PatientMyMessagesPage;
  let fixture: ComponentFixture<PatientMyMessagesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientMyMessagesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientMyMessagesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
