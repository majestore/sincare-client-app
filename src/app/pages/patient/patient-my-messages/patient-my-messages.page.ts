import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/service/http.service';
@Component({
  selector: 'app-patient-my-messages',
  templateUrl: './patient-my-messages.page.html',
  styleUrls: ['./patient-my-messages.page.scss'],
})
export class PatientMyMessagesPage implements OnInit {
  messageBox = [];
  constructor(
    private httpService: HttpService,
  ) { }

  ngOnInit() {
    this.getMessage();
  }

  public async getMessage() {
    const result: any = await this.httpService.get()
      .setPath('/message/list')
      .setQuery({
        offset: 0,
        limit: 1000
      })
      .request();

    console.log(result);
    if (result.status === 0) {
      this.messageBox = result.data.rows;
      for (const item of this.messageBox) {
        item.created_at =  new Date(item.created_at).toLocaleDateString() + ' ' +  new Date(item.created_at).toLocaleTimeString();
      }
    }
  }

}
