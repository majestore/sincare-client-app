import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import { PatientPasswordPage } from './patient-password.page';

const routes: Routes = [
  {
    path: '',
    component: PatientPasswordPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [PatientPasswordPage]
})
export class PatientPasswordPageModule {}
