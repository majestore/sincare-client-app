import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientPasswordPage } from './patient-password.page';

describe('PatientPasswordPage', () => {
  let component: PatientPasswordPage;
  let fixture: ComponentFixture<PatientPasswordPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientPasswordPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientPasswordPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
