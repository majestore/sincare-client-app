import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { PatientMyAppointmentsPage } from './patient-my-appointments.page';
import { ComponentsModule } from 'src/app/components';

const routes: Routes = [
  {
    path: '',
    component: PatientMyAppointmentsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule,
    ComponentsModule
  ],
  declarations: [PatientMyAppointmentsPage]
})
export class PatientMyAppointmentsPageModule {}
