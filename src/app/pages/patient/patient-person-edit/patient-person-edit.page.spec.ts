import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientPersonEditPage } from './patient-person-edit.page';

describe('PatientPersonEditPage', () => {
  let component: PatientPersonEditPage;
  let fixture: ComponentFixture<PatientPersonEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientPersonEditPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientPersonEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
