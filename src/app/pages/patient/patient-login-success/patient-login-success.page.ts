import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpService} from '../../../service/http.service';
import {NavController} from '@ionic/angular';

@Component({
    selector: 'app-patient-login-success',
    templateUrl: './patient-login-success.page.html',
    styleUrls: ['./patient-login-success.page.scss'],
})
export class PatientLoginSuccessPage implements OnInit {
    aPhone = '';
    aPassword = '';

    constructor(
        private navCtrl: NavController,
        private activatedRoute: ActivatedRoute,
        private httpService: HttpService) {
        activatedRoute.queryParams.subscribe(queryParams => {
            if (!!queryParams.aPhone) {
                this.aPhone = queryParams.aPhone;
            }

            if (!!queryParams.aPassword) {
                this.aPassword = queryParams.aPassword;
            }
        });
    }

    ngOnInit() {
    }

    public async toCompleteInfos() {
        const result: any = await this.httpService.post()
            .setPath('/pub/user/userLogin')
            .setBody({
                phone: this.aPhone,
                password: this.aPassword,
                userType: 1
            })
            .request();
        if (result.status === 0) {
            const loginInfo = result.data;
            localStorage.setItem('userId', result.data.userId);
            localStorage.setItem('auth-token', loginInfo.authToken);
            localStorage.setItem('redirectTo', '/patient-app');
            await this.navCtrl.navigateRoot('/patient-app');
        }
    }
}
