import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { ComponentsModule } from '../../../components';
import { PatientRegistionTimesPage } from './patient-registion-times.page';
import { TranslateModule } from '@ngx-translate/core';
import {PipeModuleModule} from '../../../pipes/pipes.module';

const routes: Routes = [
  {
    path: '',
    component: PatientRegistionTimesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule,
    TranslateModule,
    PipeModuleModule
  ],
  declarations: [
    PatientRegistionTimesPage
  ]
})
export class PatientRegistionTimesPageModule { }
