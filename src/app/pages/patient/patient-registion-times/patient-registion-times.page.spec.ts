import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientRegistionTimesPage } from './patient-registion-times.page';

describe('PatientRegistionTimesPage', () => {
  let component: PatientRegistionTimesPage;
  let fixture: ComponentFixture<PatientRegistionTimesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientRegistionTimesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientRegistionTimesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
