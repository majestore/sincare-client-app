import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from 'src/app/service/http.service';
import { AlertController, ModalController } from '@ionic/angular';
import { AgentModelComponent } from '../../../components/agent-model/agent-model.component';
import { ServiceModelComponent } from 'src/app/components/service-model/service-model.component';
import { ImgModalComponent } from 'src/app/components/img-modal/img-modal.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-patient-doctorinfo',
  templateUrl: './patient-doctorinfo.page.html',
  styleUrls: ['./patient-doctorinfo.page.scss'],
})
export class PatientDoctorinfoPage implements OnInit {
  doctorId = 0;
  doctorInfo: any = {};
  hospitalName = '';
  sysLang = '';
  patientId = '';
  constructor(
    private activatedRoute: ActivatedRoute,
    private httpService: HttpService,
    public alertController: AlertController,
    private router: Router,
    private modalController: ModalController,
    private translate: TranslateService,
  ) {
    activatedRoute.queryParams.subscribe(queryParams => {
      if (!!queryParams.doctorId) {
        this.doctorId = queryParams.doctorId;
        this.getDoctorinfo();
      }
    });
  }

  ngOnInit() {
    this.sysLang = this.translate.getBrowserCultureLang().toLowerCase();
  }

  private async getDoctorinfo() {
    const self = this;
    const result: any = await self.httpService.get()
      .setPath('/pub/user/doctorInfo/' + self.doctorId)
      .request();
    if (result.status === 0) {
      this.doctorInfo = result.data;
      this.hospitalName = this.doctorInfo.hospitalNameArray[0].hospital_name;
    }
  }

  public async toNext() {
    const aUserId = localStorage.getItem('userId');
    const result: any = await this.httpService.get()
      .setPath('/user/patientInfo/' + aUserId)
      .request();
    const aData = result.data;
    if (result.status === 0) {
      if (!aData.nickname || !aData.gender || !aData.age || !aData.medical_history || !aData.medical_desc || !aData.phone) {
        this.presentAlert();
      } else {
        if (this.doctorInfo.order_type === 1) {
          // 直接挂号，到时间预定页面
          this.router.navigate(['patient-registion-times'], {
            queryParams: { doctorId: this.doctorInfo.id }
          });
        } else {
          // 代挂号, 显示打电话Modal
          this.agencyReservation();
        }
      }
    }
  }

  public async presentAlert() {
    const aMessage = this.sysLang === 'en-us' ? 'Please complete your personal info' : '请完善个人信息之后挂号';
    const aYes = this.sysLang === 'en-us' ? 'Confirm' : '确定';
    const aNo = this.sysLang === 'en-us' ? 'Cancel' : '取消';
    const alert = await this.alertController.create({
      message: aMessage,
      buttons: [
        {
          text: aNo,
          role: 'cancel',
          handler: () => { }
        },
        {
          text: aYes,
          role: '',
          handler: () => {
            this.router.navigate(['patient-personal-center']);
          }
        }
      ]
    });
    await alert.present();
  }

  public async agencyReservation() {
    const aUserId = localStorage.getItem('userId');
    const model = await this.modalController.create({
      component: AgentModelComponent,
      cssClass: 'agent-model',
      componentProps: {
        doctorPhone: '4006261268',
        doctorId: this.doctorInfo.id,
        patientId: aUserId
      }
    });
    await model.present();
    const { data } = await model.onDidDismiss();
    if (!!data) {
      await this.onPopoverAgency();
    }
  }

  public async onPopoverAgency() {
    const model = await this.modalController.create({
      component: ServiceModelComponent,
      // cssClass: 'service-model'
    });
    await model.present();
    const { data } = await model.onDidDismiss();
  }

  public async showCH() {
    const modal = await this.modalController.create({
      component: ImgModalComponent,
      componentProps: {
        title: 'CooperativeHospitals',
        hospitalArr: this.doctorInfo.hospitalNameArray
      }
    });
    await modal.present();
  }

  public async showPI() {
    const modal = await this.modalController.create({
      component: ImgModalComponent,
      componentProps: {
        title: 'PersonInfo',
        personalInfo: this.doctorInfo.personal_desc
      }
    });
    await modal.present();
  }

  public async showMQ() {
    const modal = await this.modalController.create({
      component: ImgModalComponent,
      componentProps: {
        title: 'MedicalQualifications',
        imgId: this.doctorInfo.attachment_url
      }
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
  }

  public async showPH() {
    const modal = await this.modalController.create({
      component: ImgModalComponent,
      componentProps: {
        title: 'PersonalHonors',
        personalRewards: this.doctorInfo.personal_honor
      }
    });
    await modal.present();
  }

  public async onAgentClick() {
    this.patientId = localStorage.getItem('userId');
    const result: any = await this.httpService.post()
        .setPath('/order/createAgent')
        .setBody({
            doctorUserId: this.doctorId,
            patientUserId: this.patientId
        })
        .request();

    if (result.status === 0) {}
    await this.onPopoverAgency();
  }
}
