import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {Routes, RouterModule} from '@angular/router';

import {PatientHomePage} from './patient-home.page';
import {TranslateModule} from '@ngx-translate/core';
import {ComponentsModule} from './../../../components';

const routes: Routes = [
    {
        path: '',
        component: PatientHomePage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        TranslateModule,
        ComponentsModule
    ],
    declarations: [PatientHomePage]
})
export class PatientHomePageModule {
}
