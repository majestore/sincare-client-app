import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PatientHomePage} from './patient-home.page';

describe('PatientHomePage', () => {
    let component: PatientHomePage;
    let fixture: ComponentFixture<PatientHomePage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PatientHomePage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PatientHomePage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
