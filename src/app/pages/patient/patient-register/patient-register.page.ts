import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../service/http.service';
import { NavController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { ToastService } from '../../../service/toast.service';
import { TranslateService } from '@ngx-translate/core';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-patient-register',
    templateUrl: './patient-register.page.html',
    styleUrls: ['./patient-register.page.scss'],
})
export class PatientRegisterPage implements OnInit {
    phoneNum = '';
    phoneCode = '';
    password = '';
    confirm = '';
    emailNum = '';
    verifyCode: any = {
        verifyCodeTips: 'GetCode',
        countdown: 300,
        disable: true,
    };
    sysLang = '';
    codeArr: any[] = [
        '1',
        '86',
        '81',
        '66',
        '852',
        '886'
    ];
    countryCode = '';
    loginForAppoint = false;

    constructor(
        private activatedRoute: ActivatedRoute,
        private navCtrl: NavController,
        private httpService: HttpService,
        private toastSrv: ToastService,
        private alertController: AlertController,
        private translate: TranslateService,
    ) {
        activatedRoute.queryParams.subscribe(queryParams => {
            this.loginForAppoint = !!queryParams.loginForAppoint;
        });
    }

    ngOnInit() {
        this.sysLang = this.translate.getBrowserCultureLang().toLowerCase();
    }

    public async registerIme() {
        if (this.sysLang === 'en-us') {
            if (!this.phoneNum) {
                this.toastSrv.showToast('Please Enter Exact Phone Number');
                return;
            }
            if (!this.phoneCode) {
                this.toastSrv.showToast('Please Enter Verification Code');
                return;
            }
            if (!this.password || this.password.length < 8) {
                this.toastSrv.showToast('Password Must Be At Least 8 Characters');
                return;
            }
            if (!this.confirm || this.confirm.length < 8) {
                this.toastSrv.showToast('Please Enter Confirm Password');
                return;
            } else {
                if (this.password !== this.confirm) {
                    this.toastSrv.showToast('The Two Passwords You Typed Do Not Match');
                    return;
                }
            }
            if (!this.emailNum) {
                this.toastSrv.showToast('Pease Enter Email');
                return;
            }
        } else {
            if (!this.phoneNum) {
                this.toastSrv.showToast('请输入正确手机号');
                return;
            }
            if (!this.phoneCode) {
                this.toastSrv.showToast('请输入手机验证码');
                return;
            }
            if (!this.password || this.password.length < 8) {
                this.toastSrv.showToast('请输入至少8位密码');
                return;
            }
            if (!this.confirm || this.confirm.length < 8) {
                this.toastSrv.showToast('请输入确认密码');
                return;
            } else {
                if (this.password !== this.confirm) {
                    this.toastSrv.showToast('两次密码输入不一致');
                    return;
                }
            }
            if (!this.emailNum) {
                this.toastSrv.showToast('请输入电子邮箱');
                return;
            }
        }
        const result: any = await this.httpService.post()
            .setPath('/pub/user/patientRegister')
            .setBody({
                phone: this.phoneNum,
                verifyCode: this.phoneCode,
                password: this.password,
                confirm: this.confirm,
                email: this.emailNum
            })
            .request();
        if (result.status === 0) {
            await this.navCtrl.navigateForward(['patient-login-success'], {
                queryParams: { aPhone: this.phoneNum, aPassword: this.password }
            });
        }
    }

    public async toPatientLogin() {
        await this.navCtrl.pop();
    }

    public async getCode() {
        if (!this.countryCode) {
            const aDes = this.sysLang === 'en-us' ? 'Please Enter Country Code (CC)' : '请输入国家代码（CC）';
            this.toastSrv.showToast(aDes);
            return;
        }
        if (!this.phoneNum) {
            const aDes = this.sysLang === 'en-us' ? 'Please Enter Exact Phone Number' : '请输入正确手机号';
            this.toastSrv.showToast(aDes);
            return;
        }
        const result: any = await this.httpService.post()
            .setPath('/pub/user/verifyCode')
            .setBody({
                smsType: 1,
                type: 1,
                phone: this.phoneNum,
                country_code: this.countryCode
            })
            .request();
        if (result.status === 0) {
            if (this.sysLang === 'en-us') {
                const aWord = 'Message Sent';
                this.toastSrv.setDuration(5000).showToast(aWord);
            } else {
                const aWord = '验证码已发送';
                this.toastSrv.setDuration(5000).showToast(aWord);
            }
            if (result.data.expire < 300) {
            } else {
                this.verifyCode.disable = false;
                this.settime();
            }
        }
    }

    public settime() {
        if (this.verifyCode.countdown === 1) {
            this.verifyCode.countdown = 300;
            this.verifyCode.verifyCodeTips = 'GetCode';
            this.verifyCode.disable = true;
            return;
        } else {
            this.verifyCode.countdown--;
        }

        this.verifyCode.verifyCodeTips = (this.sysLang === 'en-us' ? 'Resend(' : '重新获取(') + this.verifyCode.countdown + ')';
        setTimeout(() => {
            this.verifyCode.verifyCodeTips = (this.sysLang === 'en-us' ? 'Resend(' : '重新获取(') + this.verifyCode.countdown + ')';
            this.settime();
        }, 1000);
    }

    public toPrivacy() {
        this.navCtrl.navigateForward(['privacy']);
    }
}
