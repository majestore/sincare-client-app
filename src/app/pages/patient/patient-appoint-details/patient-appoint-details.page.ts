import {Component, OnInit} from '@angular/core';
import {HttpService} from 'src/app/service/http.service';
import {ActivatedRoute} from '@angular/router';
import {ModalController} from '@ionic/angular';
import {ImgModalComponent} from 'src/app/components/img-modal/img-modal.component';

@Component({
    selector: 'app-patient-appoint-details',
    templateUrl: './patient-appoint-details.page.html',
    styleUrls: ['./patient-appoint-details.page.scss'],
})
export class PatientAppointDetailsPage implements OnInit {
    orderTime = '';
    doctorId = '';
    orderId = '';
    doctorInfo: any = {};
    orderStatus = 0;

    constructor(
        private httpService: HttpService,
        private activatedRoute: ActivatedRoute,
        public modalController: ModalController
    ) {
        activatedRoute.queryParams.subscribe(queryParams => {
            if (!!queryParams.doctorId) {
                this.doctorId = queryParams.doctorId;
                this.getDoctorInfo(queryParams.doctorId);
            }

            if (!!queryParams.orderTime) {
                this.orderTime = queryParams.orderTime;
            }

            if (!!queryParams.orderId) {
                this.orderId = queryParams.orderId;
            }

            if (!!queryParams.orderStatus) {
                this.orderStatus = queryParams.orderStatus;
            }

            this.readOrder(queryParams.orderId);
        });
    }

    ngOnInit() {
    }

    public async getDoctorInfo(aUserId) {
        const result: any = await this.httpService.get()
            .setPath('/user/doctorInfo/' + aUserId)
            .request();
        if (result.status === 0) {
            this.doctorInfo = result.data;
        }
    }

    public async showMQ() {
        const modal = await this.modalController.create({
            component: ImgModalComponent,
            componentProps: {
                title: '从医资质',
                imgId: this.doctorInfo.attachment_url
            }
        });
        await modal.present();
        const {data} = await modal.onDidDismiss();
    }

    public async showCH() {
        const modal = await this.modalController.create({
            component: ImgModalComponent,
            componentProps: {
                title: '合作医院',
                hospitalArr: this.doctorInfo.hospitalNameArray
            }
        });
        await modal.present();
    }

    public async readOrder(aOrderId) {
        const result: any = await this.httpService.put()
            .setPath('/order/patientRead/' + aOrderId)
            .request();
        if (result.status === 0) {

        }
    }

}
