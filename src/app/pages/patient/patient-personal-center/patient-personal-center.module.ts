import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { PatientPersonalCenterPage } from './patient-personal-center.page';
import { ComponentsModule } from '../../../components';
import { RadioPickModelComponent } from './../../../components/radio-pick-model/radio-pick-model.component';

const routes: Routes = [
  {
    path: '',
    component: PatientPersonalCenterPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule,
    ComponentsModule
  ],
  declarations: [PatientPersonalCenterPage],
  entryComponents: [
    RadioPickModelComponent
  ]
})
export class PatientPersonalCenterPageModule { }
