import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DoctorMyAppointmentsPage } from './doctor-my-appointments.page';

const routes: Routes = [
  {
    path: '',
    component: DoctorMyAppointmentsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DoctorMyAppointmentsPage]
})
export class DoctorMyAppointmentsPageModule {}
