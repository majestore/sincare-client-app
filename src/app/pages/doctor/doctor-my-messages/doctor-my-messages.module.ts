import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { DoctorMyMessagesPage } from './doctor-my-messages.page';

const routes: Routes = [
  {
    path: '',
    component: DoctorMyMessagesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [DoctorMyMessagesPage]
})
export class DoctorMyMessagesPageModule {}
