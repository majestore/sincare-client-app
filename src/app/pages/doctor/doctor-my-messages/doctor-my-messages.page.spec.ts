import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorMyMessagesPage } from './doctor-my-messages.page';

describe('DoctorMyMessagesPage', () => {
  let component: DoctorMyMessagesPage;
  let fixture: ComponentFixture<DoctorMyMessagesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorMyMessagesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorMyMessagesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
