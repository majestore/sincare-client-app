import {Component, OnInit} from '@angular/core';
import {HttpService} from '../../../service/http.service';
import {ToastService} from 'src/app/service/toast.service';
import {NavController} from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
@Component({
    selector: 'app-doctor-login',
    templateUrl: './doctor-login.page.html',
    styleUrls: ['./doctor-login.page.scss'],
})
export class DoctorLoginPage implements OnInit {
    aEmail = '';
    aPassword = '';
    emailP = 'Email';
    passwordP = 'Password';
    sysLang = '';

    constructor(
        private navController: NavController,
        private httpService: HttpService,
        private toastService: ToastService,
        private translate: TranslateService) {
    }

    ngOnInit() {
        this.sysLang = this.translate.getBrowserCultureLang().toLowerCase();
    }

    public async toDoctor() {
        if (!this.aEmail) {
            this.toastService.showToast('请输入电子邮箱');
            return;
        }

        if (!this.aPassword) {
            this.toastService.showToast('请输入密码');
            return;
        }
        const result: any = await this.httpService.post()
            .setPath('/pub/user/userLogin')
            .setBody({
                email: this.aEmail,
                password: this.aPassword,
                userType: 2
            })
            .request();

        if (result.status === 0) {
            this.aPassword = '';
            this.aEmail = '';
            const loginInfo = result.data;
            localStorage.setItem('userId', loginInfo.userId);
            localStorage.setItem('userType', loginInfo.type);
            localStorage.setItem('auth-token', loginInfo.authToken);
            localStorage.setItem('redirectTo', '/doctor-app');
            await this.navController.navigateRoot('/doctor-app');
        }
    }

    async forgetPassword() {
        await this.navController.navigateForward(['doctor-password'], {
            queryParams: {isForgetPassword: '0'}
        });
    }

    async registerNow() {
        await this.navController.navigateForward('/doctor-register');
    }

    public toPrivacy() {
        this.navController.navigateForward(['privacy']);
    }

}
