import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorLoginPage } from './doctor-login.page';

describe('DoctorLoginPage', () => {
  let component: DoctorLoginPage;
  let fixture: ComponentFixture<DoctorLoginPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorLoginPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorLoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
