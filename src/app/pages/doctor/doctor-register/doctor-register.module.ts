import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {DoctorRegisterPage} from './doctor-register.page';

import {RegionPickModelComponent} from '../../../components/region-pick-model/region-pick-model.component';
import {RegionListModelComponent} from '../../../components/region-pick-model/region-list-model/region-list-model.component';
import {ComponentsModule} from '../../../components';

const routes: Routes = [
    {
        path: '',
        component: DoctorRegisterPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        TranslateModule,
        ComponentsModule
    ],
    declarations: [
        DoctorRegisterPage
    ],
    entryComponents: [
        RegionPickModelComponent,
        RegionListModelComponent
    ]
})
export class DoctorRegisterPageModule {
}
