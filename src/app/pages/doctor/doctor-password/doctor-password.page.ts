import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {NavController, AlertController} from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import {HttpService} from '../../../service/http.service';
import {ToastService} from '../../../service/toast.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-doctor-password',
    templateUrl: './doctor-password.page.html',
    styleUrls: ['./doctor-password.page.scss'],
})
export class DoctorPasswordPage implements OnInit {
    isShow = false;
    isForgetPassword = '0';

    aEmail = '';
    aVerifyCode = '';
    aPassword = '';
    aConfirm = '';
    sysLang = '';
    verifyCode: any = {
        verifyCodeTips: 'GetCode',
        countdown: 300,
        disable: true,
    };

    constructor(
        private router: Router,
        public nav: NavController,
        private activatedRoute: ActivatedRoute,
        private httpService: HttpService,
        private toastSrv: ToastService,
        public alertController: AlertController,
        private translate: TranslateService,
    ) {
        activatedRoute.queryParams.subscribe(queryParams => {
            if (!!queryParams.isForgetPassword) {
                this.isForgetPassword = queryParams.isForgetPassword;
            }
        });
    }

    ngOnInit() {
        this.sysLang = this.translate.getBrowserCultureLang().toLowerCase();
    }

    public async complete() {
        if (this.sysLang === 'en-us') {
            if (!this.aEmail) {
                this.toastSrv.showToast('Please Enter Email');
                return;
            }
            if (!this.aVerifyCode) {
                this.toastSrv.showToast('Please Enter Verification Code');
                return;
            }
            if (!this.aPassword || this.aPassword.length < 8) {
                this.toastSrv.showToast('Password Must Be At Least 8 Characters');
                return;
            }
            if (!this.aConfirm || this.aConfirm.length < 8) {
                this.toastSrv.showToast('Please Enter Confirm Password');
                return;
            } else {
                if (this.aPassword !== this.aConfirm) {
                    this.toastSrv.showToast('The Two Passwords You Typed Do Not Match');
                    return;
                }
            }
        } else {
            if (!this.aEmail) {
                this.toastSrv.showToast('请输入邮箱号');
                return;
            }
            if (!this.aVerifyCode) {
                this.toastSrv.showToast('请输入邮箱验证码');
                return;
            }
            if (!this.aPassword || this.aPassword.length < 8) {
                this.toastSrv.showToast('请输入至少8位密码');
                return;
            }
            if (!this.aConfirm || this.aConfirm.length < 8) {
                this.toastSrv.showToast('请输入确认密码');
                return;
            } else {
                if (this.aPassword !== this.aConfirm) {
                    this.toastSrv.showToast('两次密码输入不一致');
                    return;
                }
            }
        }
        const result: any = await this.httpService.put()
            .setPath('/pub/user/resetPassword/2')
            .setBody({
                email: this.aEmail,
                verifyCode: this.aVerifyCode,
                password: this.aPassword,
                confirm: this.aConfirm
            })
            .request();
        if (result.status === 0) {
            this.isShow = true;
        }
    }

    public async confirm() {
        this.isShow = false;
        if (this.isForgetPassword === '0') {
            this.nav.navigateRoot('/login');
        } else {
            this.nav.pop();
        }
    }

    public settime() {
        if (this.verifyCode.countdown === 1) {
            this.verifyCode.countdown = 300;
            this.verifyCode.verifyCodeTips = 'GetCode';
            this.verifyCode.disable = true;
            return;
        } else {
            this.verifyCode.countdown--;
        }

        this.verifyCode.verifyCodeTips = (this.sysLang === 'en-us' ? 'Resend(' : '重新获取(') + this.verifyCode.countdown + ')';
        setTimeout(() => {
            this.verifyCode.verifyCodeTips = (this.sysLang === 'en-us' ? 'Resend(' : '重新获取(') + this.verifyCode.countdown + ')';
            this.settime();
        }, 1000);
    }

    public async getCode() {
        if (this.sysLang === 'en-us') {
            if (!this.aEmail) {
                this.toastSrv.showToast('Please Enter Email');
                return;
            }
        } else {
            if (!this.aEmail) {
                this.toastSrv.showToast('请输入邮箱号');
                return;
            }
        }
        const result: any = await this.httpService.post()
            .setPath('/pub/user/verifyCode')
            .setBody({
                smsType: 2,
                type: 2,
                email: this.aEmail,
            })
            .request();
        if (result.status === 0) {
            if (this.sysLang === 'en-us') {
                const aWord = 'Message Sent';
                this.toastSrv.setDuration(5000).showToast(aWord);
            } else {
                const aWord = '验证码已发送';
                this.toastSrv.setDuration(5000).showToast(aWord);
            }
            if (result.data.expire < 300) {
            } else {
                this.verifyCode.disable = false;
                this.settime();
            }
        }
    }
}
