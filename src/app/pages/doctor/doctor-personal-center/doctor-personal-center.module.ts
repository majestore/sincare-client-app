import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { DoctorPersonalCenterPage } from './doctor-personal-center.page';
import { ComponentsModule } from '../../../components';
const routes: Routes = [
  {
    path: '',
    component: DoctorPersonalCenterPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule,
    ComponentsModule
  ],
  declarations: [DoctorPersonalCenterPage],
  entryComponents: []
})
export class DoctorPersonalCenterPageModule { }
