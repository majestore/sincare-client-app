import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from 'src/app/service/http.service';
import {AlertController, NavController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-doctor-appoint-details',
  templateUrl: './doctor-appoint-details.page.html',
  styleUrls: ['./doctor-appoint-details.page.scss'],
})
export class DoctorAppointDetailsPage implements OnInit {
  orderTime = '';
  patientId = '';
  patientInfo: any = {};
  orderId = '';
  orderAccept = 0;
  doctorType = 2;
  sysLang = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    private httpService: HttpService,
    private alertController: AlertController,
    private nav: NavController,
    private translate: TranslateService
  ) {
    activatedRoute.queryParams.subscribe(async queryParams => {
      if (!!queryParams.startTime) {
        this.orderTime = `${queryParams.startTime} - ${queryParams.endTime.split(' ')[1]}`;
      }

      if (!!queryParams.patientId) {
        this.patientId = queryParams.patientId;
      }
      await this.getPatientInfo(queryParams.patientId);
      if (!!queryParams.orderId) {
        await this.readOrder(queryParams.orderId);
        this.orderId = queryParams.orderId;
      }

      if (!!queryParams.orderAccept) {
        this.orderAccept = Number(queryParams.orderAccept);
      }

      if (!!queryParams.doctorType) {
        this.doctorType = Number(queryParams.doctorType);
      }
    });
  }

  ngOnInit() {
    this.sysLang = this.translate.getBrowserCultureLang().toLowerCase();
  }

  public async getPatientInfo(aPatientId) {
    const result: any = await this.httpService.get()
      .setPath('/user/patientInfo/' + aPatientId)
      .request();
    if (!!result.data) {
      this.patientInfo = result.data;
    }
  }

  public async clickAccept() {
    const alertData = {
      'en-us': {
        header: 'Alert',
        message: 'Appoint Accept?',
        cancelText: 'Close',
        okText: 'Confirm'
      },
      'zh-cn': {
        header: '提示',
        message: '受理预约吗？',
        cancelText: '关闭',
        okText: '确认'
      }
    };

    const cancelAppointAlert = await this.alertController.create({
      header: alertData[this.sysLang].header,
      message: alertData[this.sysLang].message,
      buttons: [
        {
          text: alertData[this.sysLang].cancelText,
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: alertData[this.sysLang].okText,
          handler: async () => {
            const result: any = await this.httpService.put()
                .setPath('/order/accept/' + this.orderId)
                .request();
            if (result.status === 0) {
              await this.nav.pop();
            }
          }
        }
      ]
    });
    await cancelAppointAlert.present();
  }

  public async clickRefuse() {
    const alertData = {
      'en-us': {
        header: 'Alert',
        message: 'Cancel Appoint?',
        cancelText: 'Close',
        okText: 'Confirm'
      },
      'zh-cn': {
        header: '提示',
        message: '取消预约吗？',
        cancelText: '关闭',
        okText: '确认'
      }
    };

    const cancelAppointAlert = await this.alertController.create({
      header: alertData[this.sysLang].header,
      message: alertData[this.sysLang].message,
      buttons: [
        {
          text: alertData[this.sysLang].cancelText,
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: alertData[this.sysLang].okText,
          handler: async () => {
            const result: any = await this.httpService.put()
                .setPath('/order/reject/' + this.orderId)
                .setBody({
                  rejectType: 1
                })
                .request();
            if (result.status === 0) {
              await this.nav.pop();
            }
          }
        }
      ]
    });
    await cancelAppointAlert.present();
  }

  public async onAppointmentFinish() {
    const alertData = {
      'en-us': {
        header: 'Alert',
        message: 'Complete Testing?',
        cancelText: 'Close',
        okText: 'Confirm'
      },
      'zh-cn': {
        header: '提示',
        message: '完成当前检查吗？',
        cancelText: '关闭',
        okText: '确认'
      }
    };

    const cancelAppointAlert = await this.alertController.create({
      header: alertData[this.sysLang].header,
      message: alertData[this.sysLang].message,
      buttons: [
        {
          text: alertData[this.sysLang].cancelText,
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: alertData[this.sysLang].okText,
          handler: async () => {
            const result: any = await this.httpService.put()
                .setPath('/order/complete/' + this.orderId)
                .request();
            if (result.status === 0) {
              await this.nav.pop();
            }
          }
        }
      ]
    });
    await cancelAppointAlert.present();
  }

  public async readOrder(aOrderId) {
    const result: any = await this.httpService.put()
      .setPath('/order/doctorRead/' + aOrderId)
      .request();
    if (result.status === 0) {

    }
  }
}
