import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DoctorLoginSuccessPage } from './doctor-login-success.page';

const routes: Routes = [
  {
    path: '',
    component: DoctorLoginSuccessPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DoctorLoginSuccessPage]
})
export class DoctorLoginSuccessPageModule {}
