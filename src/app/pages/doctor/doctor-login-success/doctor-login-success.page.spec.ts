import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorLoginSuccessPage } from './doctor-login-success.page';

describe('DoctorLoginSuccessPage', () => {
  let component: DoctorLoginSuccessPage;
  let fixture: ComponentFixture<DoctorLoginSuccessPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorLoginSuccessPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorLoginSuccessPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
