import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-doctor-login-success',
  templateUrl: './doctor-login-success.page.html',
  styleUrls: ['./doctor-login-success.page.scss'],
})
export class DoctorLoginSuccessPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
