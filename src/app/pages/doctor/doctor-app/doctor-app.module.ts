import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DoctorAppPage } from './doctor-app.page';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'doctor-home',
        pathMatch: 'full'
    },
    {
        path: 'doctor-home',
        loadChildren: '../doctor-home/doctor-home.module#DoctorHomePageModule'
    },
    {
        path: 'doctor-personal-center',
        loadChildren: '../doctor-personal-center/doctor-personal-center.module#DoctorPersonalCenterPageModule'
    },
    {
        path: 'doctor-appointments',
        loadChildren: '../doctor-appointments/doctor-appointments.module#DoctorAppointmentsPageModule'
    },
    {
        path: 'doctor-my-appointments',
        loadChildren: '../doctor-my-appointments/doctor-my-appointments.module#DoctorMyAppointmentsPageModule'
    },
    {
        path: 'doctor-my-messages',
        loadChildren: '../doctor-my-messages/doctor-my-messages.module#DoctorMyMessagesPageModule'
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [DoctorAppPage]
})
export class DoctorAppPageModule {
}
