import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Routes, RouterModule } from '@angular/router';

import { DoctorHomePage } from './doctor-home.page';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/components';

const routes: Routes = [
    {
        path: '',
        component: DoctorHomePage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        TranslateModule,
        ComponentsModule
    ],
    declarations: [DoctorHomePage]
})
export class DoctorHomePageModule {
}
