import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { DoctorAppointmentsPage } from './doctor-appointments.page';
import { ComponentsModule } from '../../../components';
import {PipeModuleModule} from '../../../pipes/pipes.module';

const routes: Routes = [
  {
    path: '',
    component: DoctorAppointmentsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule,
    ComponentsModule,
    PipeModuleModule
  ],
  declarations: [DoctorAppointmentsPage]
})
export class DoctorAppointmentsPageModule { }
