import {Component, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {TranslateService} from '@ngx-translate/core';
import {Device} from '@ionic-native/device/ngx';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.page.html',
    styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

    constructor(
        private navCtrl: NavController,
        private storage: Storage,
        public device: Device,
        private translate: TranslateService) {

    }

    ngOnInit() {
        // alert('Device UUID is: ' + this.device.uuid);
    }

    onSelectLanguage(lang) {
        this.translate.use(lang);
        localStorage.setItem('currentLanguage', lang);
    }

    onUserLogout() {
        localStorage.removeItem('login');
        this.navCtrl.navigateForward('login');
    }
}
